﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Script_MenuWinner : MonoBehaviour
{
	public Text Text_ScoreLevel_TapMouse;
	public Text Text_ScoreLevel_Escape;
	public Text Text_ScoreLevel_Score;
	public Text Text_ScoreLevel_MoneyLevel;
	public Text Text_ScoreLevel_TotalMoney;
	//
	public GameObject _EtoileTapMouse;
	public GameObject _EtoileEscape;
	public GameObject _EtoileScore;
	//
	public GameObject _TextMoneyTapMouse;
	public GameObject _TextMoneyEscape;
	public GameObject _TextMoneyScore;
	//
	private int _NbEnnemisToT_Moustique_1;
	private int _NbEnnemisToT_Moustique_2;
	private int _NbEnnemisToT_Moustique_3;
	private int _NbEnnemisToT_Moustique_4;
	private int _NbEnnemisToT_Moustique_5;
	private int _ScoreMoustique_1;
	private int _ScoreMoustique_2;
	private int _ScoreMoustique_3;
	private int _ScoreMoustique_4;
	private int _ScoreMoustique_5;
	private bool ok = false;
	private int _MoneyLevel;
	//
	private Script_parametreGame _Script_parametreGame;

	void Start()
	{
		_Script_parametreGame = new Script_parametreGame ();
		//
		_ScoreMoustique_1 = Script_ScoreManager._ScoreMoustique_1;
		_ScoreMoustique_2 = Script_ScoreManager._ScoreMoustique_2;
		_ScoreMoustique_3 = Script_ScoreManager._ScoreMoustique_3;
		_ScoreMoustique_4 = Script_ScoreManager._ScoreMoustique_4;
		_ScoreMoustique_5 = Script_ScoreManager._ScoreMoustique_5;
		//
		if(GameObject.Find ("InvocationMoustique_1"))
			_NbEnnemisToT_Moustique_1 = GameObject.Find ("InvocationMoustique_1").GetComponent<Script_InvocationInsect> ()._Nb_EnnemisToT_Initial;
		if(GameObject.Find ("InvocationMoustique_2"))
			_NbEnnemisToT_Moustique_2 = GameObject.Find ("InvocationMoustique_2").GetComponent<Script_InvocationInsect> ()._Nb_EnnemisToT_Initial;
		if(GameObject.Find ("InvocationMoustique_3"))
			_NbEnnemisToT_Moustique_3 = GameObject.Find ("InvocationMoustique_3").GetComponent<Script_InvocationInsect> ()._Nb_EnnemisToT_Initial;
		if(GameObject.Find ("InvocationMoustique_4"))
			_NbEnnemisToT_Moustique_4 = GameObject.Find ("InvocationMoustique_4").GetComponent<Script_InvocationInsect> ()._Nb_EnnemisToT_Initial;
		if(GameObject.Find ("InvocationMoustique_5"))
			_NbEnnemisToT_Moustique_5 = GameObject.Find ("InvocationMoustique_5").GetComponent<Script_InvocationInsect> ()._Nb_EnnemisToT_Initial;

	}
	void Update ()
	{
		if (GetComponent<Canvas> ().enabled == true && ok==false)
		{
			_MoneyLevel = Script_ScoreManager._MoneyLevel;
			//**** SCORE ***//
			int _ScoreMoustiqueMax = 0;
			//if (GameObject.Find ("InvocationMoustique_1") && GameObject.Find ("InvocationMoustique_2") == false && GameObject.Find ("InvocationMoustique_3") == false) {
			if (Application.loadedLevelName == "scene Stage 1")
			{
				//_NbEnnemisToT_Moustique_1 = GameObject.Find ("InvocationMoustique_1").GetComponent<Script_InvocationInsect> ()._Nb_EnnemisToT_Initial;
				_ScoreMoustiqueMax = _ScoreMoustique_1 * _NbEnnemisToT_Moustique_1;  
				StartCoroutine (TimeLoad_Text (Text_ScoreLevel_Score, "Score: ", Script_ScoreManager._Score, ""));
				if (_ScoreMoustiqueMax <= Script_ScoreManager._Score) 
				{
					_EtoileScore.GetComponent<Animator> ().enabled = true;
					//if (Application.loadedLevelName == "scene Stage 1")
					Script_parametreLevel._Afficher_EtoileScore_Level_1 = true;
					/*if (Application.loadedLevelName == "scene Stage 2")
						Script_LevelWinANDEtoile_Manager._Afficher_EtoileScore_Level_2 = true;
					if (Application.loadedLevelName == "scene Stage 3")
						Script_LevelWinANDEtoile_Manager._Afficher_EtoileScore_Level_3 = true;*/
					_TextMoneyScore.GetComponent<Text>().enabled = true;
					StartCoroutine (TimeLoad_Text (_TextMoneyScore.GetComponent<Text>(), "",_Script_parametreGame.xMoneyScoreCOMPLET," $"));
					//Script_LevelWinANDEtoile_Manager._TotalMoney += _Script_parametreGame.xMoneyScoreCOMPLET;
					_MoneyLevel +=  _Script_parametreGame.xMoneyScoreCOMPLET;

				}
			}
			//if (GameObject.Find ("InvocationMoustique_1") && GameObject.Find ("InvocationMoustique_2") && GameObject.Find ("InvocationMoustique_3") == false) {
			if (Application.loadedLevelName == "scene Stage 2")
			{
				//_NbEnnemisToT_Moustique_1 = GameObject.Find ("InvocationMoustique_1").GetComponent<Script_InvocationInsect> ()._Nb_EnnemisToT_Initial;
				//_NbEnnemisToT_Moustique_2 = GameObject.Find ("InvocationMoustique_2").GetComponent<Script_InvocationInsect> ()._Nb_EnnemisToT_Initial;
				_ScoreMoustiqueMax = (_ScoreMoustique_1 * _NbEnnemisToT_Moustique_1) + (_ScoreMoustique_2 * _NbEnnemisToT_Moustique_2);  
				StartCoroutine (TimeLoad_Text (Text_ScoreLevel_Score, "Score: ", Script_ScoreManager._Score, ""));
				if (_ScoreMoustiqueMax <= Script_ScoreManager._Score)
				{
					_EtoileScore.GetComponent<Animator> ().enabled = true;
					/*if (Application.loadedLevelName == "scene Stage 1")
						Script_LevelWinANDEtoile_Manager._Afficher_EtoileScore_Level_1 = true;
					if (Application.loadedLevelName == "scene Stage 2")*/
					Script_parametreLevel._Afficher_EtoileScore_Level_2 = true;
					/*if (Application.loadedLevelName == "scene Stage 3")
						Script_LevelWinANDEtoile_Manager._Afficher_EtoileScore_Level_3 = true;*/
					_TextMoneyScore.GetComponent<Text>().enabled = true;
					StartCoroutine (TimeLoad_Text (_TextMoneyScore.GetComponent<Text>(), "",_Script_parametreGame.xMoneyScoreCOMPLET," $"));
					//Script_LevelWinANDEtoile_Manager._TotalMoney += _Script_parametreGame.xMoneyScoreCOMPLET;
					_MoneyLevel +=  _Script_parametreGame.xMoneyScoreCOMPLET;

				}
			}
			//if (GameObject.Find ("InvocationMoustique_1") && GameObject.Find ("InvocationMoustique_2") && GameObject.Find ("InvocationMoustique_3")) {
			if (Application.loadedLevelName == "scene Stage 3")
			{
				//_NbEnnemisToT_Moustique_1 = GameObject.Find ("InvocationMoustique_1").GetComponent<Script_InvocationInsect> ()._Nb_EnnemisToT_Initial;
				//_NbEnnemisToT_Moustique_2 = GameObject.Find ("InvocationMoustique_2").GetComponent<Script_InvocationInsect> ()._Nb_EnnemisToT_Initial;
				//_NbEnnemisToT_Moustique_3 = GameObject.Find ("InvocationMoustique_3").GetComponent<Script_InvocationInsect> ()._Nb_EnnemisToT_Initial;
				_ScoreMoustiqueMax = (_ScoreMoustique_1 * _NbEnnemisToT_Moustique_1) + (_ScoreMoustique_2 * _NbEnnemisToT_Moustique_2) + (_ScoreMoustique_3 * _NbEnnemisToT_Moustique_3);
				StartCoroutine (TimeLoad_Text (Text_ScoreLevel_Score, "Score: ", Script_ScoreManager._Score, ""));
				if (_ScoreMoustiqueMax <= Script_ScoreManager._Score)
				{
					_EtoileScore.GetComponent<Animator> ().enabled = true;
					//
					/*if (Application.loadedLevelName == "scene Stage 1")
						Script_LevelWinANDEtoile_Manager._Afficher_EtoileScore_Level_1 = true;
					if (Application.loadedLevelName == "scene Stage 2")
						Script_LevelWinANDEtoile_Manager._Afficher_EtoileScore_Level_2 = true;
					if (Application.loadedLevelName == "scene Stage 3")*/
					Script_parametreLevel._Afficher_EtoileScore_Level_3 = true;
					_TextMoneyScore.GetComponent<Text>().enabled = true;
					StartCoroutine (TimeLoad_Text (_TextMoneyScore.GetComponent<Text>(), "",_Script_parametreGame.xMoneyScoreCOMPLET," $"));
					//Script_LevelWinANDEtoile_Manager._TotalMoney += _Script_parametreGame.xMoneyScoreCOMPLET;
					_MoneyLevel +=  _Script_parametreGame.xMoneyScoreCOMPLET;

				}
			}
			if (Application.loadedLevelName == "scene Stage 4")
			{
				//_NbEnnemisToT_Moustique_1 = GameObject.Find ("InvocationMoustique_1").GetComponent<Script_InvocationInsect> ()._Nb_EnnemisToT_Initial;
				//_NbEnnemisToT_Moustique_2 = GameObject.Find ("InvocationMoustique_2").GetComponent<Script_InvocationInsect> ()._Nb_EnnemisToT_Initial;
				//_NbEnnemisToT_Moustique_3 = GameObject.Find ("InvocationMoustique_3").GetComponent<Script_InvocationInsect> ()._Nb_EnnemisToT_Initial;
				//_NbEnnemisToT_Moustique_4 = GameObject.Find ("InvocationMoustique_4").GetComponent<Script_InvocationInsect> ()._Nb_EnnemisToT_Initial;

				_ScoreMoustiqueMax = (_ScoreMoustique_1 * _NbEnnemisToT_Moustique_1) + (_ScoreMoustique_2 * _NbEnnemisToT_Moustique_2) + (_ScoreMoustique_3 * _NbEnnemisToT_Moustique_3)+ (_ScoreMoustique_4 * _NbEnnemisToT_Moustique_4);
				StartCoroutine (TimeLoad_Text (Text_ScoreLevel_Score, "Score: ", Script_ScoreManager._Score, ""));
				if (_ScoreMoustiqueMax <= Script_ScoreManager._Score) 
				{
					_EtoileScore.GetComponent<Animator> ().enabled = true;
					Script_parametreLevel._Afficher_EtoileScore_Level_4 = true;
					_TextMoneyScore.GetComponent<Text>().enabled = true;
					StartCoroutine (TimeLoad_Text (_TextMoneyScore.GetComponent<Text>(), "",_Script_parametreGame.xMoneyScoreCOMPLET," $"));
					//Script_LevelWinANDEtoile_Manager._TotalMoney += _Script_parametreGame.xMoneyScoreCOMPLET;
					_MoneyLevel +=  _Script_parametreGame.xMoneyScoreCOMPLET;

				}
			}
			if (Application.loadedLevelName == "scene Stage 5")
			{
				//_NbEnnemisToT_Moustique_1 = GameObject.Find ("InvocationMoustique_1").GetComponent<Script_InvocationInsect> ()._Nb_EnnemisToT_Initial;
				//_NbEnnemisToT_Moustique_2 = GameObject.Find ("InvocationMoustique_2").GetComponent<Script_InvocationInsect> ()._Nb_EnnemisToT_Initial;
				//_NbEnnemisToT_Moustique_3 = GameObject.Find ("InvocationMoustique_3").GetComponent<Script_InvocationInsect> ()._Nb_EnnemisToT_Initial;
				//_NbEnnemisToT_Moustique_4 = GameObject.Find ("InvocationMoustique_4").GetComponent<Script_InvocationInsect> ()._Nb_EnnemisToT_Initial;
				//_NbEnnemisToT_Moustique_5 = GameObject.Find ("InvocationMoustique_5").GetComponent<Script_InvocationInsect> ()._Nb_EnnemisToT_Initial;

				_ScoreMoustiqueMax = (_ScoreMoustique_1 * _NbEnnemisToT_Moustique_1) + (_ScoreMoustique_2 * _NbEnnemisToT_Moustique_2) + (_ScoreMoustique_3 * _NbEnnemisToT_Moustique_3)+ (_ScoreMoustique_4 * _NbEnnemisToT_Moustique_4) +  (_ScoreMoustique_5 * _NbEnnemisToT_Moustique_5);
				StartCoroutine (TimeLoad_Text (Text_ScoreLevel_Score, "Score: ", Script_ScoreManager._Score, ""));
				if (_ScoreMoustiqueMax <= Script_ScoreManager._Score) 
				{
					_EtoileScore.GetComponent<Animator> ().enabled = true;
					Script_parametreLevel._Afficher_EtoileScore_Level_5 = true;
					_TextMoneyScore.GetComponent<Text>().enabled = true;
					StartCoroutine (TimeLoad_Text (_TextMoneyScore.GetComponent<Text>(), "",_Script_parametreGame.xMoneyScoreCOMPLET," $"));
					//Script_LevelWinANDEtoile_Manager._TotalMoney += _Script_parametreGame.xMoneyScoreCOMPLET;
					_MoneyLevel +=  _Script_parametreGame.xMoneyScoreCOMPLET;
				}
			}
			//**** ESCAPE ****//
			StartCoroutine (TimeLoad_Text (Text_ScoreLevel_Escape, "Escape: ", Script_ScoreManager._NB_EscapeFinal, "/" + Script_ScoreManager._NB_EscapeInitial));
			if (Script_ScoreManager._NB_EscapeFinal == Script_ScoreManager._NB_EscapeInitial) 
			{
				_EtoileEscape.GetComponent<Animator> ().enabled = true;
				if (Application.loadedLevelName == "scene Stage 1")
				{
					Script_parametreLevel._Afficher_EtoileEscape_Level_1 = true;
				}
				if (Application.loadedLevelName == "scene Stage 2")
				{
					Script_parametreLevel._Afficher_EtoileEscape_Level_2 = true;
				}
					
				if (Application.loadedLevelName == "scene Stage 3")
				{
					Script_parametreLevel._Afficher_EtoileEscape_Level_3 = true;
				}
					
				if (Application.loadedLevelName == "scene Stage 4")
				{
					Script_parametreLevel._Afficher_EtoileEscape_Level_4 = true;
				}
					
				if (Application.loadedLevelName == "scene Stage 5")
				{
					Script_parametreLevel._Afficher_EtoileEscape_Level_5 = true;
				}
				_TextMoneyEscape.GetComponent<Text>().enabled = true;
				StartCoroutine (TimeLoad_Text (_TextMoneyEscape.GetComponent<Text>(), "",_Script_parametreGame.xMoneyEscapeCOMPLET," $"));
				//Script_LevelWinANDEtoile_Manager._TotalMoney += _Script_parametreGame.xMoneyEscapeCOMPLET;
				_MoneyLevel +=  _Script_parametreGame.xMoneyEscapeCOMPLET;

			} else {	
				/*if(Application.loadedLevelName == "scene Stage 1")
				Script_EtoileNB._NB_EtoileEscape_Level_1 = false;*/
			}
			//*****TAP MOUSE*****//
			StartCoroutine (TimeLoad_Text (Text_ScoreLevel_TapMouse, "Hits: ", Script_parametreLevel._TapMouse, "/" + Script_parametreLevel._TapMouseIN));
			if(Script_parametreLevel._TapMouse<=Script_parametreLevel._TapMouseIN)
			{
				_EtoileTapMouse.GetComponent<Animator> ().enabled = true;
				if (Application.loadedLevelName == "scene Stage 1")
				{
					Script_parametreLevel._Afficher_EtoileTapMouse_Level_1 = true;
				}
				if (Application.loadedLevelName == "scene Stage 2")
				{
					Script_parametreLevel._Afficher_EtoileTapMouse_Level_2 = true;
				}
				
				if (Application.loadedLevelName == "scene Stage 3")
				{
					Script_parametreLevel._Afficher_EtoileTapMouse_Level_3 = true;
				}

				if (Application.loadedLevelName == "scene Stage 4")
				{
					Script_parametreLevel._Afficher_EtoileTapMouse_Level_4 = true;
				}
				
				if (Application.loadedLevelName == "scene Stage 5")
				{
					Script_parametreLevel._Afficher_EtoileTapMouse_Level_5 = true;
				}
				_TextMoneyTapMouse.GetComponent<Text>().enabled = true;
				StartCoroutine (TimeLoad_Text(_TextMoneyTapMouse.GetComponent<Text>(), "",_Script_parametreGame.xMoneyTapMouseCOMPLET," $"));
			}
			//***** MONEY LEVEL*****//
			StartCoroutine (TimeLoad_Text (Text_ScoreLevel_MoneyLevel ,"Money Level:",_MoneyLevel," $"));
			//*****TOTAL MONEY*****//
			Script_parametreLevel._TotalMoney += _MoneyLevel;
			StartCoroutine (TimeLoad_Text (Text_ScoreLevel_TotalMoney , "",Script_parametreLevel._TotalMoney," $"));

			ok = true;
		}
	}

	public static IEnumerator TimeLoad_Text(Text _Text,string _chaine,int _X,string scoreMoustiqueMax)
	{	
		int _unit = _X%10;
		int _deci = _X%100-_unit;
		int _cent = _X%1000-(_deci+_unit);
		int _mill = _X%10000-(_cent+_deci+_unit);;
		_X = 0;
		while(_mill!=0)
		{
			_X+=1000;
			for (float timer = 0.09f; timer >= 0;timer -= Time.deltaTime)
				yield return 0;
			_Text.text = _chaine+_X+scoreMoustiqueMax;
			_mill-=1000;
		}
		while(_cent!=0)
		{
			_X+=100;
			for (float timer = 0.09f; timer >= 0;timer -= Time.deltaTime)
				yield return 0;
			_Text.text = _chaine+_X+scoreMoustiqueMax;
			_cent-=100;
		}
		while(_deci!=0)
		{
			_X+=10;
			for (float timer = 0.09f; timer >= 0;timer -= Time.deltaTime)
				yield return 0;
			_Text.text = _chaine+_X+scoreMoustiqueMax;
			_deci-=10;
		}
		while(_unit!=0)
		{
			_X+=1;
			for (float timer = 0.09f; timer >= 0;timer -= Time.deltaTime)
				yield return 0;
			_Text.text = _chaine+_X+scoreMoustiqueMax;
			_unit-=1;
		}
		/*for (float i=0.0f; i<=_X; i++) 
		{
			for (float timer = 0.0001f; timer >= 0;timer -= Time.deltaTime)
				yield return 0;
			_Text.text = _chaine+i+scoreMoustiqueMax;
		}*/
	}
	//
	/*public static IEnumerator TimeLoad_TextTimer(Text _Text,string _chaine,float _X)
	{	for (float i=0.0f; i<=_X; i=Time.time) 
		{
			for (float timer = 0.1f; timer >= 0; timer -= Time.deltaTime)
				yield return 0;
			_Text.text = _chaine+i;
		}
	}*/

}
