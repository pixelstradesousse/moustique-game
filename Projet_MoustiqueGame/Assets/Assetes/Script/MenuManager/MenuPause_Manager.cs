﻿using UnityEngine;
using System.Collections;

public class MenuPause_Manager : MonoBehaviour
{

	public GameObject _ButtonPause;
	public GameObject _MenuPause;


	public void ResumeLevel_P()
	{
		//_Script_Pause.Pause (false);
		Script_Pause.Pause (false);
		_MenuPause.GetComponent<Canvas> ().enabled = false;
		//_ButtonPause.SetActive (true);
	}
	public void Quitter_P()
	{
		Application.Quit();
	}
}
