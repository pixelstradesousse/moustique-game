﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Script_TextScoreAnimation : MonoBehaviour
{
	public static bool _StartTextScoreAnimation=false;
	public int _MaxSize = 60;
	public int _MinSize = 25;
	public float _Frequence = 2.5f;
	private Text _Text;


	public void Start()
	{
		_Text = GetComponent<Text> ();
	}

	void Update()
	{
		if (_StartTextScoreAnimation)
			StartCoroutine(AnimationTextScore ());
	}

	IEnumerator AnimationTextScore()
	{
		float j = 0.0f;
		while (j < _Frequence) 
		{
			j += 0.1f;
			_Text.fontSize =(int) Mathf.Lerp(_MinSize, _MaxSize, j);
			yield return 0; 
		}
		j = 0.0f;
		while (j < _Frequence) 
		{
			j += 0.1f;
			_Text.fontSize =(int) Mathf.Lerp(_MaxSize, _MinSize, j);
			yield return 0; 
		}
		_StartTextScoreAnimation = false;
	}
}
