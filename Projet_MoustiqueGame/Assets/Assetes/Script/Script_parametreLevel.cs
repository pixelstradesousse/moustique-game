﻿using UnityEngine;
using System.Collections;

public class Script_parametreLevel : MonoBehaviour 
{
	//parametres globaux

	public static bool _Afficher_EtoileTapMouse_Level_1 = false;
	public static bool _Afficher_EtoileEscape_Level_1 = false;
	public static bool _Afficher_EtoileScore_Level_1 = false;

	public static bool _Afficher_EtoileTapMouse_Level_2 = false;
	public static bool _Afficher_EtoileEscape_Level_2 = false;
	public static bool _Afficher_EtoileScore_Level_2 = false;

	public static bool _Afficher_EtoileTapMouse_Level_3 = false;
	public static bool _Afficher_EtoileEscape_Level_3 = false;
	public static bool _Afficher_EtoileScore_Level_3 = false;

	public static bool _Afficher_EtoileTapMouse_Level_4 = false;
	public static bool _Afficher_EtoileEscape_Level_4 = false;
	public static bool _Afficher_EtoileScore_Level_4 = false;

	public static bool _Afficher_EtoileTapMouse_Level_5 = false;
	public static bool _Afficher_EtoileEscape_Level_5 = false;
	public static bool _Afficher_EtoileScore_Level_5 = false;
	// Tap mouse
	public static int _TapMouse = 0;
	public static int _TapMouseIN = 0;
	// Level Win
	public static bool _WinLevel_1=false;
	public static bool _WinLevel_2=false;
	public static bool _WinLevel_3=false;
	public static bool _WinLevel_4=false;
	public static bool _WinLevel_5=false;
	// Monnaie
	public static int _TotalMoney = 0;
	//  ARme Shotgun
	public static bool _ShotgunAcheter = false;
	public static bool _ShotgunSelect = false;
	public static int _NBCartouchShotgun = 5;
	public static int _PrixShotgun = 1;
	public static int _PrixCartouche = 1;
	public static int _NBCArtouche_PAR_Achat = 20;
}
