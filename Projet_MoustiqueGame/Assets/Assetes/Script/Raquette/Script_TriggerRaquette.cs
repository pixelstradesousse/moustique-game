﻿using UnityEngine;
using System.Collections;

public class Script_TriggerRaquette : MonoBehaviour 
{
	public ParticleSystem _Etoile;

	public AudioClip _AudioC_Splash_1;
	public AudioClip _AudioC_Splash_2;
	public AudioClip _AudioC_Splash_3;
	private AudioSource _AudioS_Splash;

	private int _NB_Moustique_1_Tuer=0;
	private int _NB_Moustique_2_Tuer=0;
	private int _NB_Moustique_3_Tuer=0;
	
	private Vector3 mousePosition;
	private int _numclip=1;
	//
	public GameObject __Script_AnimationBonusScore;
	private Script_AnimationBonusScore _Script_AnimationBonusScore;

	void Awake()
	{
		_Script_AnimationBonusScore = __Script_AnimationBonusScore.GetComponent<Script_AnimationBonusScore> ();
		_AudioS_Splash = GetComponent<AudioSource> ();
	}

	IEnumerator Reinitialiser_NBMoustique()
	{
		for (float timer = 0.5f; timer >= 0; timer -= Time.deltaTime)
			yield return 0;
		 _NB_Moustique_1_Tuer=0;
		 _NB_Moustique_2_Tuer=0;
		 _NB_Moustique_3_Tuer=0;
	}

	void OnTriggerEnter2D(Collider2D coll)
	{
		mousePosition = Script_DetectClickMouse.MousePostionClick (Camera.main);//GetComponentInParent<Script_ShootingRaquette> ().mousePosition;
		float _x_mo1 = mousePosition.x - 5;
		float _x_mo2 = mousePosition.x + 5;
		
		float _y_mo1 = mousePosition.y - 5;
		float _y_mo2 = mousePosition.y + 5;
		
		float _x_mu = coll.transform.position.x;
		float _y_mu = coll.transform.position.y;
		
		//Debug.Log ("mustique x:"+_x_mu);
		//Debug.Log ("position x:"+_x_mo1+2);
		
		if (coll.gameObject.tag == "Moustique" && (_x_mu>=_x_mo1&&_x_mu<=_x_mo2 )&&(_y_mu>=_y_mo1&&_y_mu<=_y_mo2)  ) 
		{
			Debug.Log("TapIN:"+Script_parametreLevel._TapMouseIN++);
			//
			if(_numclip == 1)
			{
				_AudioS_Splash.clip = _AudioC_Splash_1 ;
				_numclip=2;
			}
			else if(_numclip == 2)
			{
				_AudioS_Splash.clip = _AudioC_Splash_2 ;
				_numclip=3;
			}
			else if(_numclip == 3)
			{
				_AudioS_Splash.clip = _AudioC_Splash_3 ;
				_numclip=1;
			}
			_AudioS_Splash.Play();

			/*coll.gameObject.GetComponent<Animator> ().SetTrigger ("Mort");
			coll.gameObject.GetComponent<Collider2D>().enabled = false;
			Script_InvocationParticule.CreationEffet(_Etoile,coll.transform.position );*/
			/**********Kif KIf DAns Script_TriggerCartouche *************/
			if (coll.gameObject.name == "Moustique_3(Clone)")
			{
				coll.GetComponent<Script_Moustique_3>()._vie-=coll.GetComponent<Script_Moustique_3>()._lesion;
				if(coll.GetComponent<Script_Moustique_3>()._vie == 0)
				{
					_NB_Moustique_3_Tuer ++;
					Script_KillManager.Fonction_Kill_Moustique3 (coll,_Etoile,_Script_AnimationBonusScore);
				}
				/*coll.gameObject.GetComponent<Script_Moustique_3_Mouvement> ().mort = true;
				Script_ScoreManager._Score += Script_ScoreManager._ScoreMoustique_3;
				//Script_LevelWinANDEtoile_Manager._TotalMoney += Script_ScoreManager._MoneyMoustique_3;
				Script_ScoreManager._MoneyLevel += Script_ScoreManager._MoneyMoustique_3;*/
			} else 
				if (coll.gameObject.name == "Moustique_2(Clone)")
			{
				coll.GetComponent<Script_Moustique_2>()._vie-=coll.GetComponent<Script_Moustique_2>()._lesion;
				if(coll.GetComponent<Script_Moustique_2>()._vie == 0)
				{
					_NB_Moustique_2_Tuer ++;
					Script_KillManager.Fonction_Kill_Moustique2 (coll,_Etoile,_Script_AnimationBonusScore);
				}
				/*coll.gameObject.GetComponent<Script_Moustique_1_2_Mouvement> ().mort = true;
				Script_ScoreManager._Score += Script_ScoreManager._ScoreMoustique_2;
				//Script_LevelWinANDEtoile_Manager._TotalMoney += Script_ScoreManager._MoneyMoustique_2;
				Script_ScoreManager._MoneyLevel += Script_ScoreManager._MoneyMoustique_2;*/

			} else
				if (coll.gameObject.name == "Moustique_1(Clone)")
			{
				Debug.Log("moustique1");
				coll.GetComponent<Script_Moustique_1>()._vie-=coll.GetComponent<Script_Moustique_1>()._lesion;
				if(coll.GetComponent<Script_Moustique_1>()._vie == 0)
				{
					_NB_Moustique_1_Tuer ++;
					Script_KillManager.Fonction_Kill_Moustique1 (coll,_Etoile,_Script_AnimationBonusScore);
				}
				/*coll.gameObject.GetComponent<Script_Moustique_1_2_Mouvement> ().mort = true;
				Script_ScoreManager._Score += Script_ScoreManager._ScoreMoustique_1;
				//Script_LevelWinANDEtoile_Manager._TotalMoney += Script_ScoreManager._MoneyMoustique_1;
				Script_ScoreManager._MoneyLevel += Script_ScoreManager._MoneyMoustique_1;*/
			}
			_Script_AnimationBonusScore.Function_AnimationMultiMoustiqueTuer(_NB_Moustique_1_Tuer,_NB_Moustique_2_Tuer,_NB_Moustique_3_Tuer,transform.position);
			/*if(coll.gameObject.GetComponent<HingeJoint2D>())
			{
				Script_parametreGame _Script_parametreGame = new Script_parametreGame();
				Script_ScoreManager._MoneyLevel += _Script_parametreGame.xMonnaieDor;
				_Script_AnimationBonusScore.Fonction_MonnaieDor(coll);
			}*/
			/***********************/
			StartCoroutine(Reinitialiser_NBMoustique());

		}
	}

}
