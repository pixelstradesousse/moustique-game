﻿using UnityEngine;
using System.Collections;

public class Script_ShootingBomb_Moustique5 : MonoBehaviour
{
	public Transform _FirePosition;
	public GameObject _BombMoustique;

	private bool _volArriere;
	private bool ok = true;

	void Update () 
	{
		_volArriere = GetComponent<Animator> ().GetCurrentAnimatorStateInfo (0).IsName ("vol-arriere");

		if (_volArriere && ok==true)
		{
			GameObject BomB = Instantiate (_BombMoustique,new Vector2(_FirePosition.transform.position.x,_FirePosition.transform.position.y) ,Quaternion.identity)as GameObject;
			ok=false;
		}
	}
}
