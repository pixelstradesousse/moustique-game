﻿using UnityEngine;
using System.Collections;

public class Script_InvocationParticule : MonoBehaviour 
{
	public static ParticleSystem CreationEffet(ParticleSystem prefab , Vector3 position)
	{
		ParticleSystem _particleSystem = Instantiate (prefab, position, Quaternion.identity) as ParticleSystem ;
		Destroy (_particleSystem.gameObject , _particleSystem.duration);//startLifetime
		return _particleSystem;
	}
}
