﻿using UnityEngine;
using System.Collections;

public class Script_Moustique_4_Mouvement : MonoBehaviour 
{
	private float _TimeDebut=0;
	private float _TimeFin=0;
	private float y;
	private bool _mort;
	//private bool _shooting;

	void Start()
	{
		//_shooting = GetComponent<Script_ShootingSeringue>()._Shooting;
	}

	void Update () 
	{
		_mort = GetComponent<Animator> ().GetCurrentAnimatorStateInfo (0).IsName ("mort");

		if (_mort == false) {
			if (Script_Pause.isPause == false) {
				_TimeDebut = Time.time;
				if ((_TimeDebut - _TimeFin) > 0.2f) {
					_TimeFin = _TimeDebut;
					y = Random.Range (0.15f, -0.15f);
				}
				transform.Translate (new Vector2 (-Time.deltaTime * 1.5f, y));
			} else {
				GetComponent<Script_ShootingSeringue> ()._Shooting = false;
			}
		} else {
			transform.Translate (new Vector2 (-Time.deltaTime * 2, Time.deltaTime * 15));
			Destroy (this.gameObject, 5.0f);
		}
	}
}
