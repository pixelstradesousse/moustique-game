﻿using UnityEngine;
using System.Collections;

public class MenuGameOver_Manager : MonoBehaviour 
{
	public GameObject _MenuGameOver;
	//public GameObject _ButtonPause;

	public void RestartLevel_GO()
	{
		//Script_Pause.Pause (false);
		_MenuGameOver.GetComponent<Canvas> ().enabled = false;
		//_ButtonPause.SetActive (true);
		Application.LoadLevel ( Application.loadedLevelName);
	}
	//
	public void Quitter_GO()
	{
		Application.Quit();
	}
}
