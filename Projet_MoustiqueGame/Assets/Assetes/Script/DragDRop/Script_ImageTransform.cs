﻿using UnityEngine;
using System.Collections;

public class Script_ImageTransform : MonoBehaviour
{
	public void Fonction_ReinitialisationIMAGE(Transform _SlotTransform)
	{
		if(_SlotTransform.childCount == 1 )
		{
			Transform _Item = _SlotTransform.GetChild(0);
			if(_Item.gameObject.name == "shotgun")
			{
				_Item.transform.SetParent(GameObject.Find("Depart_Shotgun").GetComponent<Transform>());
				Script_parametreLevel._ShotgunSelect = false;
			}
			/*if(_Item.gameObject.name == "raquette")
			{
				_Item.transform.SetParent(GameObject.Find("Depart_Raquette").GetComponent<Transform>());
			}/*
			if(_Item.gameObject.name  == "Gold 1")
			{
				_Item.transform.SetParent(GameObject.Find("Depart_Glod_1").GetComponent<Transform>());
			}
			if(_Item.gameObject.name  == "Gold 0")
			{
				_Item.transform.SetParent(GameObject.Find("Depart_Glod_0").GetComponent<Transform>());
			}*/
			_Item.gameObject.GetComponent<CanvasGroup> ().blocksRaycasts = true;
		}
	}
}
