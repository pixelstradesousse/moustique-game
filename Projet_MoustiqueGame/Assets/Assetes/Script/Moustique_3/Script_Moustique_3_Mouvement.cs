﻿using UnityEngine;
using System.Collections;

public class Script_Moustique_3_Mouvement : MonoBehaviour
{
	private float _TimeDebut=0;
	private float _TimeDebut_speed;
	private float _TimeFin=0;
	private float _TimeFin_speed;
	private float y;
	private float x;
	public float _speed=4.0f;
	public bool mort = false;

	private float _Speed;
	//
	public ParticleSystem _Explosion;

	public AudioClip _AudioC_Explosion;
	private AudioSource _AudioS_Explosion;
	//
	void Awake () 
	{
		_AudioS_Explosion = Camera.main.GetComponent<AudioSource> ();
	}

	//
	void Start () 
	{
		_Speed=1.0f;
		_TimeDebut_speed = Time.time;
	}	
	// Update is called once per frame
	void Update () 
	{
		if(mort == true)
			Destroy (gameObject,1.0f);
		if (Script_Pause.isPause == false && mort == false)
		{
			_TimeDebut = Time.time;

			if ((_TimeDebut - _TimeFin) > 0.2f)
			{
				_TimeFin = _TimeDebut;
				y = Random.Range (0.15f, -0.15f);
			}
			//*************//
			_TimeFin_speed = Time.time - _TimeDebut_speed;
			//Debug.Log ("_TimeFin_speed:"+_TimeFin_speed);
			if (_TimeFin_speed > 2.5f) 
			{
				_Speed = _speed;
			} else
				_Speed = 1.0f;

			transform.Translate (new Vector2 (-0.2f * _Speed, y));
		}
	}

	void OnCollisionEnter2D(Collision2D coll)
	{
		if (coll.gameObject.name == "Homme") 
		{
			Script_InvocationParticule.CreationEffet (_Explosion, this.transform.position);
			//
			_AudioS_Explosion.clip = _AudioC_Explosion;
			_AudioS_Explosion.Play ();
			Script_ScoreManager.Funtion_DiminutionEscape (2);
			Destroy (this.gameObject);
		}
	}
}
