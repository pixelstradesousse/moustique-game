﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScreenUI_Manager : MonoBehaviour 
{
	//public GameObject _ButtonPause;
	public GameObject _MenuPause;
	public Text Text_NBCartouche;
	public Script_parametreGame _Script_parametreGame;

	public GameObject _ArmeShotgun;
	public GameObject _ArmeRaquette;
	public GameObject _ZoneDetection_Shotgun;
	public GameObject _ZoneDetection_Raquette;
	//
	public GameObject _ButtonShotgun;
	//
	public GameObject _IconMoustique_1;
	public GameObject _IconMoustique_2;
	public GameObject _IconMoustique_3;
	public GameObject _IconMoustique_4;
	public GameObject _IconMoustique_5;
	public Text _TextLevel;
	//
	public Sprite _audio_0;
	public Sprite _audio_1;
	public Sprite _audio_2;
	public Sprite _audio_3;
	//
	private bool  _ok_Volume;
	private int _ok_ButtonVolume_ACT = 0;
	private int _ok_ButtonVolume_PRE = 0;

	void Awake()
	{
		GetComponentInParent<AudioSource> ().volume = 0.999f;
		// select shotgun si l'utilisateur l'a selectionner dans store
		if (Script_parametreLevel._ShotgunSelect == false)
		{
			_ButtonShotgun.SetActive(false);
		}
		//***********************//
		if(Application.loadedLevelName == "scene Stage 1")
		{
			_IconMoustique_2.SetActive(false);
			_IconMoustique_3.SetActive(false);
			_IconMoustique_4.SetActive(false);
			_IconMoustique_5.SetActive(false);
			_TextLevel.text = "Level 1";
		}
		if(Application.loadedLevelName == "scene Stage 2")
		{
			_IconMoustique_3.SetActive(false);
			_IconMoustique_4.SetActive(false);
			_IconMoustique_5.SetActive(false);
			_TextLevel.text = "Level 2";

		}
		if(Application.loadedLevelName == "scene Stage 3")
		{
			_IconMoustique_4.SetActive(false);
			_IconMoustique_5.SetActive(false);
			_TextLevel.text = "Level 3";
		}
		if(Application.loadedLevelName == "scene Stage 4")
		{
			_IconMoustique_5.SetActive(false);
			_TextLevel.text = "Level 4";
			
		}
		if(Application.loadedLevelName == "scene Stage 5")
		{
			_TextLevel.text = "Level 5";
			
		}
		// choisir la Raquette comme 1er arme
		ActiverArmeRaquette_UI ();

	}
	void Start()
	{

	}
	void Update()
	{
		Text_NBCartouche.text = ""+Script_parametreLevel._NBCartouchShotgun;
	}
	//****//
	public void ActiverArmeRaquette_UI()
	{
		_ZoneDetection_Shotgun.SetActive (false);
		_ZoneDetection_Raquette.SetActive (true);
		_ArmeRaquette.SetActive(true);
		_ArmeShotgun.SetActive (false);
	}
	public void ActiverArmeShotgun_UI()
	{
		_ZoneDetection_Shotgun.SetActive (true);
		_ZoneDetection_Raquette.SetActive (false);
		_ArmeRaquette.SetActive(false);
		_ArmeShotgun.SetActive (true);
	}
	/*****************************/
	public void Volume_UI(GameObject _ButtonVolume)
	{
		float _volume = GetComponentInParent<AudioSource> ().volume;
		switch(_ok_ButtonVolume_ACT)
		{
			case 0 :
			_ButtonVolume.GetComponent<Image> ().sprite = _audio_1;
			_ok_ButtonVolume_ACT = 1;
			_ok_ButtonVolume_PRE = 0;
			break;
		case 1 :
			if(_ok_ButtonVolume_PRE == 0)
			{
				_ButtonVolume.GetComponent<Image> ().sprite = _audio_2;
				_ok_ButtonVolume_ACT = 2;
			}
			if(_ok_ButtonVolume_PRE == 2)
			{
				_ButtonVolume.GetComponent<Image> ().sprite = _audio_0;
				_ok_ButtonVolume_ACT = 0;
			}
			_ok_ButtonVolume_PRE = 1;
			break;
		case 2 :
			if(_ok_ButtonVolume_PRE == 1)
			{
				_ButtonVolume.GetComponent<Image> ().sprite = _audio_3;
				_ok_ButtonVolume_ACT = 3;
			}
			if(_ok_ButtonVolume_PRE == 3)
			{
				_ButtonVolume.GetComponent<Image> ().sprite = _audio_1;
				_ok_ButtonVolume_ACT = 1;
			}
			_ok_ButtonVolume_PRE = 2;
			break;
		case 3 :
			_ButtonVolume.GetComponent<Image> ().sprite = _audio_2;
			_ok_ButtonVolume_ACT = 2;
			_ok_ButtonVolume_PRE = 3;
			break;
		}
		if (_volume >= 0.999) 
			_ok_Volume = true;
		if(_volume == 0)
			_ok_Volume = false;
		
		if(_ok_Volume)
			GetComponentInParent<AudioSource> ().volume = _volume - 0.333f;
		else
			GetComponentInParent<AudioSource> ().volume = _volume + 0.333f;
	
	}
	public void RestartLevel_UI()
	{
		//Script_Pause.Pause (false);
		//_ButtonPause.SetActive (true);
		Application.LoadLevel ( Application.loadedLevelName);
	}
	public void Pause_UI()
	{
		Script_Pause.Pause (true);
		_MenuPause.GetComponent<Canvas> ().enabled = true;
	}


}
