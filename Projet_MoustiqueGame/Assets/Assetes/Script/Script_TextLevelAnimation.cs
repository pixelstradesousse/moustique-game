﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Script_TextLevelAnimation : MonoBehaviour
{
	private bool ok=true;
	private Text _Text;
	public float _frequenceTransparence = 0.0f; // seconde
	public int _frequenceFontSize=4; // font size

	public void Start()
	{
		_Text = GetComponent<Text> ();
		StartCoroutine(FadeOutCR());
	}
	
	 IEnumerator FadeOutCR()
	{
		while (ok) 
		{
			for (float timer = 0.001f; timer >= 0; timer -= Time.deltaTime)
				yield return 0;
			_Text.fontSize += _frequenceFontSize;
			if(_Text.fontSize>200)
				ok=false;
		}
		for (float timer = 1; timer >= 0; timer -= Time.deltaTime)
			yield return 0;
		float duration = _frequenceTransparence; 
		float currentTime = 0f;
		while(currentTime <= duration)
		{
			float alpha = Mathf.Lerp(1f, 0f, currentTime/duration);// transparence 
			_Text.color = new Color(_Text.color.r, _Text.color.g, _Text.color.b, alpha);
			currentTime += Time.deltaTime;
			yield return null;
		}
		yield break;
	}
}
