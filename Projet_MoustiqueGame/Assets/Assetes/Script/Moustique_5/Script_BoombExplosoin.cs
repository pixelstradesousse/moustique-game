﻿using UnityEngine;
using System.Collections;

public class Script_BoombExplosoin : MonoBehaviour 
{

	public AudioClip _AudioC_Explosion;
	private AudioSource _AudioS_Explosion;
	

	void Awake () 
	{
		_AudioS_Explosion = Camera.main.GetComponent<AudioSource> ();
	}

	void OnCollisionEnter2D(Collision2D coll)
	{
		if(coll.gameObject.name == "Homme")
		{
			this.gameObject.GetComponent<Animator> ().SetTrigger ("explosion");
			this.gameObject.GetComponent<CircleCollider2D>().enabled = false;
			_AudioS_Explosion.clip = _AudioC_Explosion;
			_AudioS_Explosion.Play();
			//
			Script_ScoreManager.Funtion_DiminutionEscape(5);
			//
			Destroy (this.gameObject,0.6f);
		}
	}
}
