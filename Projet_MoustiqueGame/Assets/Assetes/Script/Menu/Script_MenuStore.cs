﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Script_MenuStore : MonoBehaviour
{
	public Material _ImageNonDispo;
	// *Shotgun*
	public GameObject _Shotgun;
	public Button _ButtonAchat_Shotgun;
	public Button _ButtonAchatCartouche_Shotgun;
	public Button _ButtonPuissance_Shotgun;
	public Text _TextPrixShotgun;
	public Text _TextPrixCartouche;
	public Text _TextNBCartouche_PAR_Achat;
	// **
	private bool _ok = false;
	
	void Start () 
	{
		Fonction_FixerLesPrix();

	}
	void Update () 
	{
		if (GetComponent<Canvas>().enabled == true) 
		{
			if(_ok == false)
			{
				Fonction_ManagerShotgun ();
				//IsSelected_Shotgun (_Shotgun);
				_ok = true;
			}
		}
	}

	//
	void Fonction_FixerLesPrix()
	{
		/*Prix Shotgun et Cartouche*/
		_TextPrixShotgun.text = Script_parametreLevel._PrixShotgun + " $";
		_TextPrixCartouche.text = Script_parametreLevel._PrixCartouche + " $";
		_TextNBCartouche_PAR_Achat.text = "+"+Script_parametreLevel._NBCArtouche_PAR_Achat;
		/**/
		
	}
	/**Manager Shotgun**/
	void Fonction_ManagerShotgun()
	{

		if (Script_parametreLevel._WinLevel_1 == false || Script_parametreLevel._WinLevel_2 == false)
		{
			//if(_ok3==false)
			//{
			GameObject.Find("Text_NBCartoucheShotgun").GetComponent<Text>().enabled = false;
				_ButtonAchat_Shotgun.interactable = false;
				_ButtonAchatCartouche_Shotgun.interactable = false;
				_ButtonPuissance_Shotgun.interactable = false;
				_Shotgun.GetComponent<Image> ().material = _ImageNonDispo;
				_Shotgun.GetComponent<CanvasGroup> ().blocksRaycasts = false;
				//_ok3 = true;
			//}
		}else 
		{
			if(Script_parametreLevel._TotalMoney < Script_parametreLevel._PrixShotgun)
			{
				//if(_ok4==false)
				//{
					_ButtonAchat_Shotgun.interactable = false;
					//_ok4=true;
				//}
			}
			if(Script_parametreLevel._ShotgunAcheter == false)
			{
				//if(_ok1==false)
				//{	
				GameObject.Find("Text_NBCartoucheShotgun").GetComponent<Text>().enabled = false;
					_ButtonAchatCartouche_Shotgun.interactable = false;
					_ButtonPuissance_Shotgun.interactable = false;
					_Shotgun.GetComponent<CanvasGroup> ().blocksRaycasts = false;
					_Shotgun.GetComponent<Image> ().material = null;
					Fonction_Transparence(_Shotgun,0.5f);
					//_ok1 = true;
				//}
			}else
			//if (_ok2==false)
			{
				_ButtonAchat_Shotgun.interactable = false;
				//_ok2 =true;
				if(Script_parametreLevel._ShotgunSelect == true)
				{
					_Shotgun.transform.SetParent(GameObject.Find("Arriver_Shotgun").GetComponent<Transform>());
					_Shotgun.GetComponent<CanvasGroup> ().blocksRaycasts = false;
				}
			}
		}
	}

	public void Achat_Object(GameObject _Objet)
	{
		if(_Objet.name =="shotgun")
		{
			Script_parametreLevel._TotalMoney -= Script_parametreLevel._PrixShotgun;
			Script_parametreLevel._ShotgunAcheter = true;
			_Objet.GetComponent<CanvasGroup> ().blocksRaycasts = true;
			Fonction_Transparence(_Objet,1);
			//
			_ButtonPuissance_Shotgun.interactable = true;
			_ButtonAchatCartouche_Shotgun.interactable = true;
			GameObject.Find("Text_NBCartoucheShotgun").GetComponent<Text>().enabled = true;
		}
	}
	public void Achat_Munition(GameObject _Objet)
	{
		if(_Objet.name == "ButtonAchatCartouch")
		{
			if(Script_parametreLevel._TotalMoney >= Script_parametreLevel._PrixCartouche)
			{
				Script_parametreLevel._TotalMoney -= Script_parametreLevel._PrixCartouche;
				Script_parametreLevel._NBCartouchShotgun += Script_parametreLevel._NBCArtouche_PAR_Achat;
			}

		}
	}
	//
	void Fonction_Transparence(GameObject _Arme,float _alpha)
	{
		Image _ImageArme = _Arme.GetComponent<Image> ();
		Color _ColorShotgun = _ImageArme.color; 
		_ColorShotgun.a = _alpha;
		_ImageArme.color = _ColorShotgun;
	}
}
