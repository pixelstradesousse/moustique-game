﻿using UnityEngine;
using System.Collections;

public class MenuWinner_Manager : MonoBehaviour
{
	public GameObject _MenuMap;
	public GameObject _MenuStoreBackground;
	public GameObject _MenuStore;

	public void ActiverMenuMap_W()
	{
		_MenuMap.GetComponent<Canvas> ().enabled = true;
		//StartCoroutine(Script_TimeLoading.TimeLoadMenu(_MenuMap));
	}
	public void NextLevel_W()
	{
		string _SceneSuivante = "";
		if (Application.loadedLevelName == "scene Stage 1")
			_SceneSuivante = "scene Stage 2";
		if(Application.loadedLevelName == "scene Stage 2")
			_SceneSuivante = "scene Stage 3";
		if(Application.loadedLevelName == "scene Stage 3")
			_SceneSuivante = "scene Stage 4";
		if(Application.loadedLevelName == "scene Stage 4")
			_SceneSuivante = "scene Stage 5";
		if(Application.loadedLevelName == "scene Stage 5")
			_SceneSuivante = "scene Menu Principal";
		StartCoroutine(Script_TimeLoading.TimeLoadLevel(_SceneSuivante));
	}
	public void RestartLevel_W()
	{
		StartCoroutine(Script_TimeLoading.TimeLoadLevel(Application.loadedLevelName));
	}
	public void Home_W()
	{
		StartCoroutine(Script_TimeLoading.TimeLoadLevel("scene Menu Principal"));
	}
	public void ActiverMenuStore_W()
	{
		_MenuStore.GetComponent<Canvas> ().enabled = true;
		_MenuStoreBackground.GetComponent<Canvas> ().enabled = true;
	}
}
