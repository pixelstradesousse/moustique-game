﻿using UnityEngine;
using System.Collections;

public class MenuDEmarrage_Manager : MonoBehaviour 
{
	public GameObject _MenuStore;
	public GameObject _MenuStoreBackground;

	public void Play_D(string _Scene)
	{
		//StartCoroutine(Script_TimeLoading.TimeLoadLevel(_Scene));
		Application.LoadLevel (_Scene);
	}
	public void Quitter_D()
	{
		Application.Quit();
	}
	public void Store_D()
	{
		_MenuStore.GetComponent<Canvas> ().enabled = true;
		_MenuStoreBackground.GetComponent<Canvas> ().enabled = true;
	}
}
