﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Script_FumerAnimation : MonoBehaviour 
{
	public float _frequenceTransparence = 1.0f; // seconde
	private SpriteRenderer _Fumer;
	private Transform _FirePostion;
	void Start () 
	{
		_FirePostion = GameObject.Find ("FirePosition").GetComponent<Transform> ();
		transform.eulerAngles = _FirePostion.eulerAngles;
		_Fumer = GetComponentInChildren<SpriteRenderer> ();
		StartCoroutine (Animation());
	}
	
	IEnumerator Animation()
	{
		float duration = _frequenceTransparence; 
		float currentTime = 0f;
		while(currentTime <= duration )
		{
			if(Script_Pause.isPause == false)
			{
				float alpha = Mathf.Lerp(1f, 0f, currentTime/duration);// transparence 
				_Fumer.color = new Color(_Fumer.color.r, _Fumer.color.g, _Fumer.color.b, alpha);
				transform.localScale += new Vector3(0.04F, 0.04F, 0);
			}
			currentTime += Time.deltaTime;
			yield return null;
		}
		yield break;
	}
}
