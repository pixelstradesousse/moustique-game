﻿using UnityEngine;
using System.Collections;

public class Script_InvocationObject : MonoBehaviour 
{
	public static GameObject InvocationObject(GameObject _Prefab,Transform _Transform)
	{
		return Instantiate (_Prefab,new Vector2(_Transform.position.x,_Transform.position.y) ,Quaternion.identity) as GameObject;
	}
}
