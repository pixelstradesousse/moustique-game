﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class Script_DragHendler : MonoBehaviour , IBeginDragHandler , IDragHandler , IEndDragHandler
{

	//
	public static GameObject itemBeingDragged;
	Vector3 startPosition;
	Transform startParent;

	#region IBeginDragHandler implementation

	public void OnBeginDrag (PointerEventData eventData)
	{
		itemBeingDragged = gameObject;
		startPosition = transform.position;
		startParent = transform.parent;
		//
		itemBeingDragged.transform.SetParent (GameObject.Find ("MenuStore").GetComponent<Canvas>().transform);//.GetComponent<Canvas>().transform);
		//
		GetComponent<CanvasGroup> ().blocksRaycasts = false;
		/*
		itemBeingDragged = gameObject;
		startPosition = transform.position;
		startParent = GameObject.Find("MenuStore").GetComponent<Canvas>().transform;
		itemBeingDragged.transform.SetParent (startParent);
		GetComponent<CanvasGroup> ().blocksRaycasts = false;*/
	}

	#endregion
	//*******************/
	#region IDragHandler implementation
	public void OnDrag (PointerEventData eventData)
	{
		transform.position = Input.mousePosition;
	}
	#endregion
	//******************/
	#region IEndDragHandler implementation

	public void OnEndDrag (PointerEventData eventData)
	{
		//itemBeingDragged = null;
		if (transform.parent == GameObject.Find ("MenuStore").GetComponent<Canvas>().transform)//.GetComponent<Canvas>().transform)
		{
			itemBeingDragged.transform.SetParent (startParent);
			GetComponent<CanvasGroup> ().blocksRaycasts = true;// ne pas fixer l'image
		} else 
		{
			GetComponent<CanvasGroup> ().blocksRaycasts = false;// fixer l'image
		}
		//
		if (Script_DragHendler.itemBeingDragged.name == "shotgun")
			Script_parametreLevel._ShotgunSelect = true;
	}

	#endregion
}
