﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MenuStore_Manager : MonoBehaviour 
{
	public GameObject _MenuStoreBackground;
	public GameObject _MenuStore;
	//
	public Button _ButtonWeapons;
	public Button _ButtonExplosives;
	public Button _ButtonDefense;
	//
	public GameObject _MenuWeapons;
	public GameObject _MenuExplosives;
	public GameObject _MenuDefense;
	//
	public Text _TextMonnaie;


	Color ImageAlpha(Button _Button,int _alpha)
	{
		Image _image_W = _Button.GetComponent<Image> ();
		Color _color_w =  _image_W.color;
		_color_w.a = _alpha;
		return _color_w;
	}

	/*******/
	public void Activer_MenuWeapons()
	{
		_ButtonWeapons.GetComponent <Image> ().color = ImageAlpha (_ButtonWeapons,1);
		_ButtonExplosives .GetComponent <Image> ().color = ImageAlpha (_ButtonWeapons,0);
		_ButtonDefense .GetComponent <Image> ().color = ImageAlpha (_ButtonWeapons,0);
		_MenuWeapons.SetActive (true);
		_MenuExplosives.SetActive (false);
		_MenuDefense.SetActive (false);
	}
	public void Activer_MenuExplosives()
	{
		_ButtonWeapons.GetComponent <Image> ().color = ImageAlpha (_ButtonWeapons,0);
		_ButtonExplosives .GetComponent <Image> ().color = ImageAlpha (_ButtonWeapons,1);
		_ButtonDefense .GetComponent <Image> ().color = ImageAlpha (_ButtonWeapons,0);
		_MenuWeapons.SetActive (false);
		_MenuExplosives.SetActive (true);
		_MenuDefense.SetActive (false);
	}
	public void Activer_MenuDefense()
	{
		_ButtonWeapons.GetComponent <Image> ().color = ImageAlpha (_ButtonWeapons,0);
		_ButtonExplosives .GetComponent <Image> ().color = ImageAlpha (_ButtonWeapons,0);
		_ButtonDefense .GetComponent <Image> ().color = ImageAlpha (_ButtonWeapons,1);
		_MenuWeapons.SetActive (false);
		_MenuExplosives.SetActive (false);
		_MenuDefense.SetActive (true);
	}
	public void DesactiverMenuStore()
	{
		_MenuStore.GetComponent<Canvas> ().enabled = false;
		_MenuStoreBackground.GetComponent<Canvas> ().enabled = false;
	}
	void Start () 
	{
		Activer_MenuWeapons ();
	}
	
	void Update () 
	{
		_TextMonnaie.text = Script_parametreLevel._TotalMoney+" $";
	}
}
