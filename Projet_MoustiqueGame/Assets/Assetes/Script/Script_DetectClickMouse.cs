﻿using UnityEngine;
using System.Collections;

public class Script_DetectClickMouse : MonoBehaviour
{

	public static bool ZoneDetection_Shotgun()
	{
		Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
		RaycastHit2D hit = Physics2D.GetRayIntersection (ray, Mathf.Infinity);
		if (hit.collider && hit.collider.name == "ZoneDetection_Shotgun") 
		{
			Debug.Log ("ZoneDetection_ClickShotgun");
			return true;
		}
		return false;
	}
	//
	public static bool ZoneDetection_Raquette()
	{
		Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
		RaycastHit2D hit = Physics2D.GetRayIntersection (ray, Mathf.Infinity);
		if (hit.collider && hit.collider.name == "ZoneDetection_Raquette") 
		{
			Debug.Log ("ZoneDetection_Raquette");
			return true;
		}
		return false;
	}
	//
	public static Vector3 MousePostionClick(Camera camera)
	{
		Vector3 mousePosition =  camera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x,Input.mousePosition.y, Input.mousePosition.z - camera.transform.position.z));
		return mousePosition;
	}
}
