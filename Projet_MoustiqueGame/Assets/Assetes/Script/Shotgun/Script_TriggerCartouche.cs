﻿using UnityEngine;
using System.Collections;

public class Script_TriggerCartouche : MonoBehaviour 
{
	public GameObject __Script_AnimationBonusScore;
	public ParticleSystem _Etoile;
	public ParticleSystem _Debris;
	public GameObject _Fire;
	
	private int _NB_Moustique_1_Tuer=0;
	private int _NB_Moustique_2_Tuer=0;
	private int _NB_Moustique_3_Tuer=0;
	private int _NB_Moustique_5_Tuer=0;
	//
	private Script_AnimationBonusScore _Script_AnimationBonusScore;

	void Awake()
	{
		_Script_AnimationBonusScore = __Script_AnimationBonusScore.GetComponent<Script_AnimationBonusScore> ();
	}

	void OnTriggerExit2D(Collider2D coll)
	{

		if (coll.gameObject.tag == "Moustique")
		{
			/*coll.gameObject.GetComponent<Animator> ().SetTrigger ("Mort");
			coll.gameObject.GetComponent<Collider2D> ().enabled = false;
			Script_InvocationParticule.CreationEffet (_Etoile, coll.transform.position);*/
			/********Kif KIf DAns Script_TriggerDetection ************/
			if (coll.gameObject.name == "Moustique_3(Clone)")
			{
				coll.GetComponent<Script_Moustique_3>()._vie-=coll.GetComponent<Script_Moustique_3>()._lesion;
				if(coll.GetComponent<Script_Moustique_3>()._vie == 0)
				{
					_NB_Moustique_3_Tuer ++;
					Script_KillManager.Fonction_Kill_Moustique3 (coll,_Etoile,_Script_AnimationBonusScore);
				}
				/*coll.gameObject.GetComponent<Script_Moustique_3_Mouvement> ().mort = true;
				Script_ScoreManager._Score += Script_ScoreManager._ScoreMoustique_3;
				//Script_LevelWinANDEtoile_Manager._TotalMoney += Script_ScoreManager._MoneyMoustique_3;
				Script_ScoreManager._MoneyLevel += Script_ScoreManager._MoneyMoustique_3;*/

			} else 
			if (coll.gameObject.name == "Moustique_2(Clone)")
			{
				Debug.Log("moustique2");
				coll.GetComponent<Script_Moustique_2>()._vie-=coll.GetComponent<Script_Moustique_2>()._lesion;
				if(coll.GetComponent<Script_Moustique_2>()._vie == 0)
				{
					_NB_Moustique_2_Tuer ++;
					Script_KillManager.Fonction_Kill_Moustique2 (coll,_Etoile,_Script_AnimationBonusScore);
				}
				/*coll.gameObject.GetComponent<Script_Moustique_1_2_Mouvement> ().mort = true;
				Script_ScoreManager._Score += Script_ScoreManager._ScoreMoustique_2;
				//Script_LevelWinANDEtoile_Manager._TotalMoney+= Script_ScoreManager._MoneyMoustique_2;
				Script_ScoreManager._MoneyLevel += Script_ScoreManager._MoneyMoustique_2;*/

			} else
			if (coll.gameObject.name == "Moustique_1(Clone)")
			{
				Debug.Log("moustique1x");
				coll.GetComponent<Script_Moustique_1>()._vie-=coll.GetComponent<Script_Moustique_1>()._lesion;
				if(coll.GetComponent<Script_Moustique_1>()._vie == 0)
				{
					_NB_Moustique_1_Tuer ++;
					Script_KillManager.Fonction_Kill_Moustique1 (coll,_Etoile,_Script_AnimationBonusScore);
				}
				/*coll.gameObject.GetComponent<Script_Moustique_1_2_Mouvement> ().mort = true;
				Script_ScoreManager._Score += Script_ScoreManager._ScoreMoustique_1;
				//Script_LevelWinANDEtoile_Manager._TotalMoney += Script_ScoreManager._MoneyMoustique_1;
				Script_ScoreManager._MoneyLevel += Script_ScoreManager._MoneyMoustique_1;*/

			}
			_Script_AnimationBonusScore.Function_AnimationMultiMoustiqueTuer(_NB_Moustique_1_Tuer,_NB_Moustique_2_Tuer,_NB_Moustique_3_Tuer,transform.position);
			/*if(coll.gameObject.GetComponent<HingeJoint2D>())
			{
				Script_parametreGame _Script_parametreGame = new Script_parametreGame();
				Script_ScoreManager._MoneyLevel += _Script_parametreGame.xMonnaieDor;
				_Script_AnimationBonusScore.Fonction_MonnaieDor(coll);
			}*/
			/*********************/
		}
		//
		if(coll.gameObject.name == "Scudo")//moustique 4
		{
			Script_InvocationParticule.CreationEffet(_Debris,coll.gameObject.transform.position);
			Destroy(this.gameObject);
		}
		if(coll.gameObject.name == "Corde")//moustique 4
		{
			GameObject Fire = Script_InvocationObject.InvocationObject(_Fire,coll.gameObject.transform);
			coll.gameObject.AddComponent<HingeJoint2D>();
			HingeJoint2D _hingeJoint2D = coll.gameObject.GetComponent<HingeJoint2D>();
			_hingeJoint2D.connectedBody = Fire.GetComponent<Rigidbody2D>();
			coll.gameObject.GetComponentInParent<Animator>().SetTrigger("Mort");
			coll.gameObject.GetComponent<BoxCollider2D>().enabled = false;
			Destroy(this.gameObject);
		}
		//
		if (coll.gameObject.name == "Moustique_5(Clone)") 
		{
			_NB_Moustique_5_Tuer ++;
			coll.gameObject.GetComponent<Script_Moustique_5_Mouvement> ().mort = true;
			//Script_ScoreManager._Score += Script_ScoreManager._ScoreMoustique_5;
		}
	}
	/*IEnumerator xxx (GameObject _Moustique4) 
	{
		yield return new WaitForSeconds(3);

	}*/
}
