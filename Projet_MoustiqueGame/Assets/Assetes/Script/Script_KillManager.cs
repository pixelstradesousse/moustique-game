﻿using UnityEngine;
using System.Collections;

public class Script_KillManager : MonoBehaviour 
{
	

	public static void Fonction_Kill_Moustique1(Collider2D coll,ParticleSystem _Etoile,Script_AnimationBonusScore _Script_AnimationBonusScore)
	{
		coll.gameObject.GetComponent<Animator> ().SetTrigger ("Mort");
		coll.gameObject.GetComponent<Collider2D>().enabled = false;
		Script_InvocationParticule.CreationEffet(_Etoile,coll.transform.position );
		//
		coll.gameObject.GetComponent<Script_Moustique_1_Mouvement> ().mort = true;
		Script_ScoreManager._Score += Script_ScoreManager._ScoreMoustique_1;
		//Script_LevelWinANDEtoile_Manager._TotalMoney += Script_ScoreManager._MoneyMoustique_1;
		Script_ScoreManager._MoneyLevel += Script_ScoreManager._MoneyMoustique_1;
		//
		if(coll.gameObject.GetComponent<HingeJoint2D>())
		{
			Script_parametreGame _Script_parametreGame = new Script_parametreGame();
			Script_ScoreManager._MoneyLevel += _Script_parametreGame.xMonnaieDor;
			_Script_AnimationBonusScore.Fonction_MonnaieDor(coll);
		}
	}

	public static void Fonction_Kill_Moustique2(Collider2D coll,ParticleSystem _Etoile,Script_AnimationBonusScore _Script_AnimationBonusScore)
	{
		coll.gameObject.GetComponent<Animator> ().SetTrigger ("Mort");
		coll.gameObject.GetComponent<Collider2D>().enabled = false;
		Script_InvocationParticule.CreationEffet(_Etoile,coll.transform.position );
		//
		coll.gameObject.GetComponent<Script_Moustique_2_Mouvement> ().mort = true;
		Script_ScoreManager._Score += Script_ScoreManager._ScoreMoustique_2;
		//Script_LevelWinANDEtoile_Manager._TotalMoney += Script_ScoreManager._MoneyMoustique_2;
		Script_ScoreManager._MoneyLevel += Script_ScoreManager._MoneyMoustique_2;
		//
		if(coll.gameObject.GetComponent<HingeJoint2D>())
		{
			Script_parametreGame _Script_parametreGame = new Script_parametreGame();
			Script_ScoreManager._MoneyLevel += _Script_parametreGame.xMonnaieDor;
			_Script_AnimationBonusScore.Fonction_MonnaieDor(coll);
		}
	}

	public static void Fonction_Kill_Moustique3(Collider2D coll,ParticleSystem _Etoile,Script_AnimationBonusScore _Script_AnimationBonusScore)
	{
		coll.gameObject.GetComponent<Animator> ().SetTrigger ("Mort");
		coll.gameObject.GetComponent<Collider2D>().enabled = false;
		Script_InvocationParticule.CreationEffet(_Etoile,coll.transform.position );
		//
		coll.gameObject.GetComponent<Script_Moustique_3_Mouvement> ().mort = true;
		Script_ScoreManager._Score += Script_ScoreManager._ScoreMoustique_3;
		//Script_LevelWinANDEtoile_Manager._TotalMoney += Script_ScoreManager._MoneyMoustique_3;
		Script_ScoreManager._MoneyLevel += Script_ScoreManager._MoneyMoustique_3;
		//
		if(coll.gameObject.GetComponent<HingeJoint2D>())
		{
			Script_parametreGame _Script_parametreGame = new Script_parametreGame();
			Script_ScoreManager._MoneyLevel += _Script_parametreGame.xMonnaieDor;
			_Script_AnimationBonusScore.Fonction_MonnaieDor(coll);
		}
	}
}
