﻿using UnityEngine;
using System.Collections;

public class Script_ShootingSeringue : MonoBehaviour
{
	public Rigidbody2D _Seringue_1;
	public Rigidbody2D _Seringue_2;
	public Rigidbody2D _Seringue_3;

	public Transform firePosition_1;
	public Transform firePosition_2;
	public Transform firePosition_3;
	//
	public bool _Shooting = true;
	//
	public float _FrequenceShooting = 10;//seconde

	IEnumerator Start()
	{
		yield return StartCoroutine(MyDelayMethod(_FrequenceShooting));
	}
	
	IEnumerator MyDelayMethod(float delay)
	{
		while (_Shooting) {

			Rigidbody2D Seringue_1 = Instantiate (_Seringue_1, new Vector2 (firePosition_1.transform.position.x, firePosition_1.transform.position.y), Quaternion.identity)as Rigidbody2D;
			//yield return new WaitForSeconds (delay);
			Rigidbody2D Seringue_2 = Instantiate (_Seringue_2, new Vector2 (firePosition_2.transform.position.x, firePosition_2.transform.position.y), Quaternion.identity)as Rigidbody2D;
			//yield return new WaitForSeconds (delay);
			Rigidbody2D Seringue_3 = Instantiate (_Seringue_3, new Vector2 (firePosition_3.transform.position.x, firePosition_3.transform.position.y), Quaternion.identity)as Rigidbody2D;
			yield return new WaitForSeconds (delay);
		}
	}
}
