﻿using UnityEngine;
using System.Collections;

public class Script_ShootingShotgun : MonoBehaviour
{
	public Camera camera ;
	public float _speedTire=1.0f;
	public float _speedRetour=1.0f;
	public GameObject _Target;
	public Vector3 mousePosition;
	public Rigidbody2D _CartouchPrefab;
	public Rigidbody2D _FumerPrefab;
	public Transform firePosition;
	public AudioClip _AudioC_Fusil;

	//
	public Script_parametreGame _Script_parametreGame;
	private AudioSource _AudioS_Fusil;
	private float distanceFromObject;
	private Vector3 direction;
	private Script_InvocationParticule _InvocationParticule;
	//
	private bool _ok = true;
	void Awake ()
	{
		_AudioS_Fusil = GetComponent<AudioSource> ();
		_InvocationParticule = new Script_InvocationParticule ();
	}
	
	void FixedUpdate ()
	{
		Shoot();
	}

	void Shoot ()
	{
		Vector3 targetPoint = _Target.transform.position;
		Quaternion targetRotation = Quaternion.LookRotation(targetPoint - transform.position, Vector3.up);
		transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, Time.deltaTime * _speedRetour);    
		mousePosition = camera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x,Input.mousePosition.y, Input.mousePosition.z - camera.transform.position.z));
		if(_ok == true && Input.GetButtonDown("Fire1") && Script_parametreLevel._NBCartouchShotgun > 0 && Script_DetectClickMouse.ZoneDetection_Shotgun() == true)
		{
			_ok = false;
			_AudioS_Fusil.clip = _AudioC_Fusil ;
			_AudioS_Fusil.Play();
			//Rotates toward the mouse
			GetComponent<Rigidbody2D>().transform.eulerAngles = new Vector3(0,0,Mathf.Atan2((mousePosition.y - transform.position.y), (mousePosition.x - transform.position.x))*Mathf.Rad2Deg +4);
			//Judge the distance from the object and the mouse
			distanceFromObject = (Input.mousePosition - camera.WorldToScreenPoint( transform.position)).magnitude;
			//Move towards the mouse
			GetComponent<Rigidbody2D>().AddForce(direction * _speedTire * distanceFromObject * Time.deltaTime);
			//
			Rigidbody2D _Cartouche = Instantiate (_CartouchPrefab,new Vector2(firePosition.transform.position.x,firePosition.transform.position.y) ,Quaternion.identity)as Rigidbody2D;
			Instantiate (_FumerPrefab,new Vector2(firePosition.transform.position.x,firePosition.transform.position.y) ,Quaternion.identity);
			Script_parametreLevel._NBCartouchShotgun--;
			StartCoroutine (Reinitialiser_ok ());
		}
	}
	IEnumerator Reinitialiser_ok()
	{
		for (float timer = 0.1f;timer >= 0; timer -= Time.deltaTime)
			yield return 0;
		_ok = true;
	}
}
