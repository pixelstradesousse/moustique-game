﻿using UnityEngine;
using System.Collections;

public class Script_SeringueMouvementRotation : MonoBehaviour 
{
	public GameObject _Fire;

	private Transform _FirePosition;

	void Start () 
	{

		if (gameObject.name == "seringue_1(Clone)")
		{
			_FirePosition = GameObject.Find ("seringue_FirePosition_1").GetComponent<Transform> ();
		}else
		if (gameObject.name == "seringue_2(Clone)")
		{
			_FirePosition = GameObject.Find ("seringue_FirePosition_2").GetComponent<Transform> ();
		}else
		if (gameObject.name == "seringue_3(Clone)")
		{
			_FirePosition = GameObject.Find ("seringue_FirePosition_3").GetComponent<Transform> ();
		}

		transform.eulerAngles = _FirePosition.eulerAngles;
	}
	
	void Update () 
	{
		transform.Translate (new Vector2 (-Time.deltaTime * 15 , 0));

	}
	void OnCollisionEnter2D(Collision2D coll)
	{
		if (coll.gameObject.name == "Homme") 
		{
			Script_InvocationObject.InvocationObject (_Fire, this.transform);
			Script_ScoreManager.Funtion_DiminutionEscape (1);
			Destroy (this.gameObject);
		}
	}
}
