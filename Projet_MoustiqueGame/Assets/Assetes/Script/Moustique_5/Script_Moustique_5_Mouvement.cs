﻿using UnityEngine;
using System.Collections;

public class Script_Moustique_5_Mouvement : MonoBehaviour 
{
	private bool _volArriere;
	public bool mort = false;

	void Start () 
	{
		Destroy(gameObject,5);
	}
	
	void Update () 
	{
		if(mort == true)
			Destroy (gameObject,1.0f);
		if (Script_Pause.isPause == false && mort == false)
		{
			_volArriere = GetComponent<Animator> ().GetCurrentAnimatorStateInfo (0).IsName ("vol-arriere");
			if (_volArriere == false)
				transform.Translate (new Vector2 (-Time.deltaTime * 30.0f, Time.deltaTime * 15.0f));
			else 
				transform.Translate (new Vector2 (-Time.deltaTime * -30.0f, Time.deltaTime * 5.0f));

		}
	}


}
