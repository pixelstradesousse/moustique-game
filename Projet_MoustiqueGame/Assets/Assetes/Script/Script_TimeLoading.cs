﻿using UnityEngine;
using System.Collections;

public class Script_TimeLoading : MonoBehaviour
{
	public static IEnumerator TimeLoadLevel(string _StageLevel)
	{	
		for (float timer = 0.5f; timer >= 0; timer -= Time.deltaTime)
			yield return 0;
		Application.LoadLevel (_StageLevel);
	}
	//
	public static IEnumerator TimeLoadMenu(GameObject _Menu)
	{	
		for (float timer = 1.0f; timer >= 0; timer -= Time.deltaTime)
			yield return 0;
		_Menu.GetComponent<Canvas> ().enabled = true;
	}
	//
	public void LoadLevel(string _StageLevel)
	{	
		Application.LoadLevel (_StageLevel);
	}

}
