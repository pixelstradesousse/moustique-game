﻿using UnityEngine;
using System.Collections;

public class Script_AnimationBonusScore : MonoBehaviour 
{
	public ParticleSystem _Monnaie_2;
	public ParticleSystem _Monnaie_3;
	public ParticleSystem _Monnaie_4;
	//
	public ParticleSystem _BonBon1;
	public ParticleSystem _BonBon2;
	public ParticleSystem _BonBon3;
	public ParticleSystem _BonBon4;
	public ParticleSystem _BonBon5;
	//
	public Transform _TransformBonBon1;
	public Transform _TransformBonBon2;
	public Transform _TransformBonBon3;
	public Transform _TransformBonBon4;
	public Transform _TransformBonBon5;

	public void Animation_BomBom()
	{
		Script_InvocationParticule.CreationEffet(_BonBon1,_TransformBonBon1.position);
		Script_InvocationParticule.CreationEffet(_BonBon2,_TransformBonBon2.position);
		Script_InvocationParticule.CreationEffet(_BonBon3,_TransformBonBon3.position);
		Script_InvocationParticule.CreationEffet(_BonBon4,_TransformBonBon4.position);
		Script_InvocationParticule.CreationEffet(_BonBon5,_TransformBonBon5.position);
	}

	public void Function_AnimationMultiMoustiqueTuer(int _NB_Moustique_1_Tuer,int _NB_Moustique_2_Tuer,int _NB_Moustique_3_Tuer
	                               					,Vector3 _PositionMoustique)
	{
		if(_NB_Moustique_3_Tuer == 2)
		{
			Script_InvocationParticule.CreationEffet (_Monnaie_2, _PositionMoustique);
			Script_ScoreManager._Score += Script_ScoreManager._ScoreMoustique_3 * 2;
			//Script_TextScoreAnimation._StartTextScoreAnimation = true;
		}
		if(_NB_Moustique_3_Tuer == 3)
		{
			Script_ScoreManager._Score += Script_ScoreManager._ScoreMoustique_3 * 3;
			Script_InvocationParticule.CreationEffet (_Monnaie_3, _PositionMoustique);
			Animation_BomBom();
		}
		if(_NB_Moustique_3_Tuer == 4)
		{
			Script_ScoreManager._Score += Script_ScoreManager._ScoreMoustique_3 * 4;
			Script_InvocationParticule.CreationEffet (_Monnaie_4, _PositionMoustique);
			Animation_BomBom();
		}
		//
		if(_NB_Moustique_2_Tuer == 2)
		{
			Script_InvocationParticule.CreationEffet (_Monnaie_2, _PositionMoustique);
			Script_ScoreManager._Score += Script_ScoreManager._ScoreMoustique_2 * 2;
		}
		if(_NB_Moustique_2_Tuer == 3)
		{
			Script_ScoreManager._Score += Script_ScoreManager._ScoreMoustique_2 * 3;
			Script_InvocationParticule.CreationEffet (_Monnaie_3, _PositionMoustique);
			Animation_BomBom ();
		}
		if(_NB_Moustique_2_Tuer == 4)
		{
			Script_ScoreManager._Score += Script_ScoreManager._ScoreMoustique_2 * 4;
			Script_InvocationParticule.CreationEffet (_Monnaie_4, _PositionMoustique);
			Animation_BomBom ();
		}
		//
		if(_NB_Moustique_1_Tuer == 2)
		{
			Script_InvocationParticule.CreationEffet (_Monnaie_2,_PositionMoustique);
			Script_ScoreManager._Score += Script_ScoreManager._ScoreMoustique_1 * 2;
		}
		if(_NB_Moustique_1_Tuer == 3)
		{
			Script_ScoreManager._Score += Script_ScoreManager._ScoreMoustique_1 * 3;
			Script_InvocationParticule.CreationEffet (_Monnaie_3, _PositionMoustique);
			Animation_BomBom ();
		}
		if(_NB_Moustique_1_Tuer == 4)
		{
			Script_ScoreManager._Score += Script_ScoreManager._ScoreMoustique_1 * 4;
			Script_InvocationParticule.CreationEffet (_Monnaie_4, _PositionMoustique);
			Animation_BomBom ();
		}
		Script_TextScoreAnimation._StartTextScoreAnimation = true;
	}
	//
	public void Fonction_MonnaieDor(Collider2D _moustique)
	{
		GameObject _monnaieDor = _moustique.gameObject.GetComponent<HingeJoint2D>().connectedBody.gameObject;
		_monnaieDor.gameObject.GetComponentInChildren<ParticleSystem>().Play();
		_monnaieDor.GetComponent<Script_Destroy> ().enabled = true;
		Script_TextMonnaieAnimation._StartTextScoreAnimation = true;
	}
}
