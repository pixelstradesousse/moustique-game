﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class Script_ScoreManager : MonoBehaviour
{	
	//
	public static int _MoneyLevel;
	public static int _Score;
	public static int _NB_EscapeFinal;
	public static int _NB_EscapeInitial;
	//
	public Text TextMoney;
	public Text TextScore;                      // Reference to the Text component.
	public Text TextNB_Escape;
	Text TextNBMoustique_1;
	Text TextNBMoustique_2;
	Text TextNBMoustique_3;
	Text TextNBMoustique_4;
	Text TextNBMoustique_5;
	//
	private float _startTime;
	private float _deltaTime = 3.0f;
	//
	private Script_parametreGame _Script_parametreGame;
	//**//
	public static int _ScoreMoustique_1;
	public static int _ScoreMoustique_2;
	public static int _ScoreMoustique_3;
	public static int _ScoreMoustique_4;
	public static int _ScoreMoustique_5;
	//
	public static int _MoneyMoustique_1;
	public static int _MoneyMoustique_2;
	public static int _MoneyMoustique_3;
	public static int _MoneyMoustique_4;//pas encore
	public static int _MoneyMoustique_5;//pas encore
	//**//
	void Start ()
	{
		_Script_parametreGame = new Script_parametreGame ();
		_MoneyMoustique_1 = _Script_parametreGame.xMoneyMoustique_1;
		_MoneyMoustique_2 = _Script_parametreGame.xMoneyMoustique_2;
		_MoneyMoustique_3 = _Script_parametreGame.xMoneyMoustique_3;
		_MoneyMoustique_3 = _Script_parametreGame.xMoneyMoustique_4;//pas encore
		_MoneyMoustique_5 = _Script_parametreGame.xMoneyMoustique_5;//pas encore
		//
		_ScoreMoustique_1 = _Script_parametreGame.xScoreMoustique_1;
		_ScoreMoustique_2 = _Script_parametreGame.xScoreMoustique_2;
		_ScoreMoustique_3 = _Script_parametreGame.xScoreMoustique_3;
		_ScoreMoustique_3 = _Script_parametreGame.xScoreMoustique_4;
		_ScoreMoustique_5 = _Script_parametreGame.xScoreMoustique_5;
		//
		_NB_EscapeFinal = _Script_parametreGame._NB_Escape;
		_NB_EscapeInitial = _Script_parametreGame._NB_Escape;
		//
		Script_parametreLevel._TapMouse = 0;
		Script_parametreLevel._TapMouseIN = 0;
		_MoneyLevel=0;
		_Score = 0;

		if (GameObject.Find("IconMoustique_1"))
		{
			TextNBMoustique_1 = GameObject.Find ("TextMoustique_1").GetComponent<Text>();
			TextNBMoustique_1.text = "" + GameObject.Find ("InvocationMoustique_1").GetComponent<Script_InvocationInsect>()._Nb_EnnemisToT_Initial;
		}
		if (GameObject.Find("IconMoustique_2")) 
		{
			TextNBMoustique_2 = GameObject.Find ("TextMoustique_2").GetComponent<Text>();
			TextNBMoustique_2.text = "" + GameObject.Find ("InvocationMoustique_2").GetComponent<Script_InvocationInsect>()._Nb_EnnemisToT_Initial;
		}
		
		if (GameObject.Find("IconMoustique_3")) 
		{
			TextNBMoustique_3 = GameObject.Find ("TextMoustique_3").GetComponent<Text>();
			TextNBMoustique_3.text = "" +  GameObject.Find ("InvocationMoustique_3").GetComponent<Script_InvocationInsect>()._Nb_EnnemisToT_Initial;
		}
		if (GameObject.Find("IconMoustique_4")) 
		{
			TextNBMoustique_4 = GameObject.Find ("TextMoustique_4").GetComponent<Text>();
			TextNBMoustique_4.text = "" +  GameObject.Find ("InvocationMoustique_4").GetComponent<Script_InvocationInsect>()._Nb_EnnemisToT_Initial;
		}
		if (GameObject.Find("IconMoustique_5")) 
		{
			TextNBMoustique_5 = GameObject.Find ("TextMoustique_5").GetComponent<Text>();
			TextNBMoustique_5.text = "" +  GameObject.Find ("InvocationMoustique_5").GetComponent<Script_InvocationInsect>()._Nb_EnnemisToT_Initial;
		}

	}
	void Update ()
	{
		// Set the displayed text to be the word "Score" followed by the score value.
		//Script_LevelWinANDEtoile_Manager._TotalMoney += _MoneyLevel;
		TextMoney.text = ""+_MoneyLevel+" $";
		TextScore.text = "Score: " + _Score;
		TextNB_Escape.text = ""+_NB_EscapeFinal;

		_startTime = Time.time;
		if(_Score>0 && _Score%10==0 &&_startTime-_deltaTime > 3.0f )
		{
			_deltaTime = _startTime;
			if(_Score==10)
			{
				TextScore.color = Color.yellow;
			}else if(_Score==20)
			{
				TextScore.color = Color.green;
			}else if(_Score==30)
			{
				TextScore.color = Color.red;
			}
		}
	}

	public static void Funtion_DiminutionEscape(int _ValeurCollision)
	{
		if(_NB_EscapeFinal > 0)
		{
			_NB_EscapeFinal -=_ValeurCollision;
			if(_NB_EscapeFinal < 0)
				_NB_EscapeFinal=0;
		}
	}
}