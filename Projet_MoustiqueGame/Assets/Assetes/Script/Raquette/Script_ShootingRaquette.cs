﻿using UnityEngine;
using System.Collections;

public class Script_ShootingRaquette : MonoBehaviour 
{	
	//Public Vars
	//public Camera camera;
	public float speed;
	public GameObject _Target;
	public AudioClip _AudioC_Swing;
	//Private Vars
	public Vector3 mousePosition;
	private Vector3 direction;// obligatoire
	private float distanceFromObject;
	private AudioSource _AudioS_Swing;
	//
	private bool _ok = true;

	void Awake()
	{
		_AudioS_Swing = GetComponent<AudioSource> ();
	}
	void FixedUpdate() 
	{	
		Vector3 targetPoint = _Target.transform.position;
		Quaternion targetRotation = Quaternion.LookRotation(targetPoint - transform.position, Vector3.up);
		transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, Time.deltaTime * 2.0f);    

		//Debug.Log ("x:"+mousePosition.x);
		//Grab the current mouse position on the screen
		mousePosition = Script_DetectClickMouse.MousePostionClick (Camera.main);// camera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x,Input.mousePosition.y, Input.mousePosition.z - camera.transform.position.z));

		if (_ok==true && Input.GetButtonDown("Fire1") && Script_DetectClickMouse.ZoneDetection_Raquette()==true)//(mousePosition.x>=-25 && mousePosition.x <=4 ))
		{
			_ok = false;
			Debug.Log("Tap:"+Script_parametreLevel._TapMouse++);
			_AudioS_Swing.clip = _AudioC_Swing ;
			_AudioS_Swing.Play();
			//Rotates toward the mouse
			GetComponent<Rigidbody2D>().transform.eulerAngles = new Vector3(0,0,Mathf.Atan2((mousePosition.y - transform.position.y), (mousePosition.x - transform.position.x))*Mathf.Rad2Deg - 60);
			//Judge the distance from the object and the mouse
			distanceFromObject = (Input.mousePosition - Camera.main.WorldToScreenPoint( transform.position)).magnitude;
			//Move towards the mouse
			GetComponent<Rigidbody2D>().AddForce(direction * speed * distanceFromObject * Time.deltaTime);
			StartCoroutine (Reinitialiser_ok ());
		}

	}
	IEnumerator Reinitialiser_ok()
	{
		for (float timer = 0.1f; timer >= 0; timer -= Time.deltaTime)
			yield return 0;
		_ok = true;
	}
}
