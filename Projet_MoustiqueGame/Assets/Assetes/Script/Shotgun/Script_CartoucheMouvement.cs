﻿using UnityEngine;
using System.Collections;

public class Script_CartoucheMouvement : MonoBehaviour
{
	public ParticleSystem _Debris;
	private Transform _FirePosition;
	private Vector3 mouseposition;

	void Start () 
	{
		_FirePosition = GameObject.Find ("FirePosition").GetComponent<Transform> ();
		//Vector3 _rotation = _FirePosition.eulerAngles;
		//_rotation.z += 0;
		transform.eulerAngles = _FirePosition.eulerAngles;
		mouseposition = Script_DetectClickMouse.MousePostionClick (Camera.main);
	}
	
	// Update is called once per frame
	void FixedUpdate () 
	{
		transform.Translate (new Vector2 (Time.deltaTime * 150 , 0));
		if (transform.position.x >= mouseposition.x) 
		{
			//*Creatio Effet Collision Cartouche*//
			/*Script_InvocationParticule.CreationEffet(_Debris,mouseposition);
			Destroy(gameObject);*/
		}
	}
}
