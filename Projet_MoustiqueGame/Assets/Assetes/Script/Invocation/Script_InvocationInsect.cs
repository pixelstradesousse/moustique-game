﻿using UnityEngine;
using System.Collections;

public class Script_InvocationInsect : MonoBehaviour 
{
	public GameObject _GameObject;
	public float y_min;
	public float y_max;
	public int _Nb_EnnemisToT_Initial=10;//nb d'ennemie total dans le jeux
	public int _nb_EnnemisToT_Final;
	public int _Nb_EnnemisEmis=5;// nb d'ennemie par flux
	public float _PauseDebut = 0.0f;
	public float _PauseVagueEnnemis=5;//pause en seconde entre chaque vague d'ennemis enmis
	public float _frequance=1;//_frequance en seconde entre chaque moustique;
	//
	public GameObject _MonnaieDor;
	private int _indice_moustique;

	void Start () 
	{
		_nb_EnnemisToT_Final = _Nb_EnnemisToT_Initial;
		//invovatino chaque _Intensite seconde!
		// attendre 5 seconde pour la 1er invocation!
		StartCoroutine (InstansiationGameObject_Ramdom());
		_indice_moustique=Fonction_RandomMonnaieDor_Moustique ();
	}
	IEnumerator InstansiationGameObject_Ramdom()
	{	
		for (float timer = _PauseDebut; timer >= 0; timer -= Time.deltaTime)
			yield return 0;
		while(true && (_nb_EnnemisToT_Final >0) )
		{
			for (float timer = _PauseVagueEnnemis; timer >= 0; timer -= Time.deltaTime)
				yield return 0;
			for(int nb=1 ; nb<=_Nb_EnnemisEmis ; nb++)
			{
				float y = Random.Range (y_min,y_max);
				if(Script_Pause.isPause == false)
				{
					GameObject _moustique = Instantiate (_GameObject,new Vector2(transform.position.x,y) ,Quaternion.identity)as GameObject;
					if(_nb_EnnemisToT_Final == _indice_moustique)
					{
						GameObject MonnaieDor = Script_InvocationObject.InvocationObject(_MonnaieDor,_moustique.gameObject.transform);
						_moustique.gameObject.AddComponent<HingeJoint2D>();
						HingeJoint2D _hingeJoint2D = _moustique.gameObject.GetComponent<HingeJoint2D>();
						_hingeJoint2D.connectedBody = MonnaieDor.GetComponent<Rigidbody2D>();
					}
					_nb_EnnemisToT_Final--;
					for (float timer = _frequance; timer >= 0; timer -= Time.deltaTime)
						yield return 0;
				}
			}
		}
	}
	int Fonction_RandomMonnaieDor_Moustique()
	{
		int indice_moustique = Random.Range (1, _Nb_EnnemisToT_Initial);
		Debug.Log ("indice:"+indice_moustique);
		return indice_moustique;

	}
}
