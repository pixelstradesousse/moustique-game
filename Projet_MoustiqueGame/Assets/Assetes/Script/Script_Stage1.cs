﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Script_Stage1 : MonoBehaviour 
{
	public Image _ImageWin;
	public Sprite _WinSprite;
	public GameObject _MenuGameOver;
	public GameObject _MenuWinner;
	//

	//private Script_ScoreManager _Script_ScoreManager;
	
	void Start () 
	{
		//_Script_ScoreManager = GameObject.Find ("Menu").GetComponent<Script_ScoreManager> ();
	}
	
	void Update () 
	{
		if (Script_ScoreManager._NB_EscapeFinal==0)//_Script_ScoreManager._NB_Escape == 0)
		{
			GameObject.Find ("Homme").GetComponentInChildren<Script_ShootingRaquette> ().enabled = false;
			Script_Pause.isPause = true;
			StartCoroutine(Script_TimeLoading.TimeLoadMenu(_MenuGameOver));
		}else 
		{
			if (GameObject.Find("InvocationMoustique_1").GetComponent<Script_InvocationInsect>()._nb_EnnemisToT_Final <= 0) 
			{
				if(GameObject.Find("Moustique_1(Clone)")==false)
				{
					Script_parametreLevel._WinLevel_1 = true;
					_ImageWin.enabled=true;
					_ImageWin.sprite = _WinSprite;
					// ajout arme !!!!
					if(GameObject.Find ("Homme").GetComponentInChildren<Script_ShootingRaquette> ())
						GameObject.Find ("Homme").GetComponentInChildren<Script_ShootingRaquette> ().enabled = false;
					if(GameObject.Find ("Homme").GetComponentInChildren<Script_ShootingShotgun> ())
						GameObject.Find ("Homme").GetComponentInChildren<Script_ShootingShotgun> ().enabled = false;
					//
					StartCoroutine(Script_TimeLoading.TimeLoadMenu(_MenuWinner));
					//StartCoroutine(Script_TimeLoading.TimeLoadLevel("scene Stage 2"));
				}
			}
		}
	}
}
