﻿using UnityEngine;
using System.Collections;

public class Script_ZoneDetection_Moustique5 : MonoBehaviour 
{
	void OnTriggerEnter2D(Collider2D coll)
	{
		if (coll.gameObject.name == "Moustique_5(Clone)") 
		{
			coll.gameObject.GetComponent<Animator>().SetTrigger("vol-arriere");
		}
	}

}
