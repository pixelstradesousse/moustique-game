﻿using UnityEngine;
using System.Collections;

public class Script_Moustique_1_Mouvement : MonoBehaviour 
{
	private float _TimeDebut=0;
	private float _TimeFin=0;
	private float y;
	public bool mort = false;
	//
	public ParticleSystem _Boom;
	//

	void Update () 
	{

		if(mort == true)
			Destroy (gameObject,1.0f);
		if (Script_Pause.isPause == false && mort == false)
		{
			_TimeDebut = Time.time;
			if ((_TimeDebut - _TimeFin) > 0.2f) 
			{
				_TimeFin = _TimeDebut;
				y = Random.Range (0.15f, -0.15f);
			}
			transform.Translate (new Vector2 (-0.2f, y));
		}
	}

	void OnCollisionEnter2D(Collision2D coll)
	{
		if (coll.gameObject.name == "Homme") 
		{
			Script_InvocationParticule.CreationEffet (_Boom, this.transform.position);
			//
			Script_ScoreManager.Funtion_DiminutionEscape (1);
			Destroy (this.gameObject);
		}
	}
}
