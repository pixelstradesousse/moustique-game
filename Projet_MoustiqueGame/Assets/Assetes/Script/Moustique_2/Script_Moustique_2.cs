﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Script_Moustique_2 : MonoBehaviour 
{
	public float _lesion = 0.5f;//lesion par coup
	public float _vie = 1; // vie moustique 1
	//
	public Image _BarreVie;
	
	void Update()
	{
		_BarreVie.fillAmount = _vie;
		
	}
}
