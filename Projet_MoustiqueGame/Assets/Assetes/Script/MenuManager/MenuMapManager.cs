﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MenuMapManager : MonoBehaviour 
{
	public GameObject _EtoileEscape_Level1_MAP;
	public GameObject _EtoileScore_Level1_MAP;
	public GameObject _EtoileTapMouse_Level1_MAP;

	public GameObject _EtoileEscape_Level2_MAP;
	public GameObject _EtoileScore_Level2_MAP;
	public GameObject _EtoileTapMouse_Level2_MAP;

	public GameObject _EtoileEscape_Level3_MAP;
	public GameObject _EtoileScore_Level3_MAP;
	public GameObject _EtoileTapMouse_Level3_MAP;

	public GameObject _EtoileEscape_Level4_MAP;
	public GameObject _EtoileScore_Level4_MAP;
	public GameObject _EtoileTapMouse_Level4_MAP;

	public GameObject _EtoileEscape_Level5_MAP;
	public GameObject _EtoileScore_Level5_MAP;
	public GameObject _EtoileTapMouse_Level5_MAP;
	//
	public Button Button_StartLevel1;
	public Button Button_StartLevel2;
	public Button Button_StartLevel3;
	public Button Button_StartLevel4;
	public Button Button_StartLevel5;
	//
	private bool ok =false;
	public GameObject _MenuMap;

	void Start()
	{
		//Image x = _EtoileEscape_Level1_MAP.GetComponent<Image> ();
		//x.enabled = true;
	}
	void Update()
	{

		if (_MenuMap.GetComponent<Canvas> ().enabled == false)
			ok = false;
		if (_MenuMap.GetComponent<Canvas> ().enabled == true && ok==false)
		{
			// Activation
			if(Script_parametreLevel._WinLevel_1 == true)
			{
				Button_StartLevel2.interactable = true;
				if (Script_parametreLevel._Afficher_EtoileEscape_Level_1)
					_EtoileEscape_Level1_MAP.GetComponent<Image>().enabled = true;
				if (Script_parametreLevel._Afficher_EtoileScore_Level_1)
					_EtoileScore_Level1_MAP.GetComponent<Image>().enabled = true;
				if (Script_parametreLevel._Afficher_EtoileTapMouse_Level_1)
					_EtoileTapMouse_Level1_MAP.GetComponent<Image>().enabled = true;
			}
			if(Script_parametreLevel._WinLevel_2 == true)
			{
				Button_StartLevel3.interactable = true;
				if (Script_parametreLevel._Afficher_EtoileEscape_Level_2)
					_EtoileEscape_Level2_MAP.GetComponent<Image>().enabled = true;
				if (Script_parametreLevel._Afficher_EtoileScore_Level_2)
					_EtoileScore_Level2_MAP.GetComponent<Image>().enabled = true;
				if (Script_parametreLevel._Afficher_EtoileTapMouse_Level_2)
					_EtoileTapMouse_Level2_MAP.GetComponent<Image>().enabled = true;
			}
			if(Script_parametreLevel._WinLevel_3 == true)
			{
				Button_StartLevel4.interactable = true;
				if (Script_parametreLevel._Afficher_EtoileEscape_Level_3)
					_EtoileEscape_Level3_MAP.GetComponent<Image>().enabled = true;
				if (Script_parametreLevel._Afficher_EtoileScore_Level_3)
					_EtoileScore_Level3_MAP.GetComponent<Image>().enabled = true;
				if (Script_parametreLevel._Afficher_EtoileTapMouse_Level_3)
					_EtoileTapMouse_Level3_MAP.GetComponent<Image>().enabled = true;
			}
			if(Script_parametreLevel._WinLevel_4 == true)
			{
				Button_StartLevel5.interactable = true;
				if (Script_parametreLevel._Afficher_EtoileEscape_Level_4)
					_EtoileEscape_Level4_MAP.GetComponent<Image>().enabled = true;
				if (Script_parametreLevel._Afficher_EtoileScore_Level_4)
					_EtoileScore_Level4_MAP.GetComponent<Image>().enabled = true;
				if (Script_parametreLevel._Afficher_EtoileTapMouse_Level_4)
					_EtoileTapMouse_Level4_MAP.GetComponent<Image>().enabled = true;
			}
			// Animation
			if(Application.loadedLevelName == "scene Stage 1")
			{
				if (Script_parametreLevel._Afficher_EtoileEscape_Level_1 &&
				    _EtoileEscape_Level1_MAP.GetComponent<Animator> ().enabled==false)
				{
					_EtoileEscape_Level1_MAP.GetComponent<Animator> ().enabled = true;
				}
				if (Script_parametreLevel._Afficher_EtoileScore_Level_1 &&
				    _EtoileScore_Level1_MAP.GetComponent<Animator> ().enabled==false)
				{
					_EtoileScore_Level1_MAP.GetComponent<Animator> ().enabled=true;
				}
				if (Script_parametreLevel._Afficher_EtoileTapMouse_Level_1 &&
				    _EtoileTapMouse_Level1_MAP.GetComponent<Animator> ().enabled==false)
				{
					_EtoileTapMouse_Level1_MAP.GetComponent<Animator> ().enabled=true;
				}
			}
			if(Application.loadedLevelName == "scene Stage 2")
			{
				if (Script_parametreLevel._Afficher_EtoileEscape_Level_2 &&
				    _EtoileEscape_Level2_MAP.GetComponent<Animator> ().enabled==false)
				{
					_EtoileEscape_Level2_MAP.GetComponent<Animator> ().enabled=true;
				}
				if (Script_parametreLevel._Afficher_EtoileScore_Level_2 &&
				    _EtoileScore_Level2_MAP.GetComponent<Animator> ().enabled==false)
				{
					_EtoileScore_Level2_MAP.GetComponent<Animator> ().enabled=true;
				}
				if (Script_parametreLevel._Afficher_EtoileTapMouse_Level_2 &&
				    _EtoileTapMouse_Level2_MAP.GetComponent<Animator> ().enabled==false)
				{
					_EtoileTapMouse_Level2_MAP.GetComponent<Animator> ().enabled=true;
				}
			}
			if(Application.loadedLevelName == "scene Stage 3")
			{
				if (Script_parametreLevel._Afficher_EtoileEscape_Level_3 &&
				    _EtoileEscape_Level3_MAP.GetComponent<Animator> ().enabled == false)
				{
					_EtoileEscape_Level3_MAP.GetComponent<Animator> ().enabled=true;
				}
				if (Script_parametreLevel._Afficher_EtoileScore_Level_3 &&
				    _EtoileScore_Level3_MAP.GetComponent<Animator> ().enabled==false)
				{
					_EtoileScore_Level3_MAP.GetComponent<Animator> ().enabled=true;
				}
				if (Script_parametreLevel._Afficher_EtoileTapMouse_Level_3 &&
				    _EtoileTapMouse_Level3_MAP.GetComponent<Animator> ().enabled==false)
				{
					_EtoileTapMouse_Level3_MAP.GetComponent<Animator> ().enabled=true;
				}
			}
			if(Application.loadedLevelName == "scene Stage 4")
			{
				if (Script_parametreLevel._Afficher_EtoileEscape_Level_4 &&
				    _EtoileEscape_Level4_MAP.GetComponent<Animator> ().enabled==false)
				{
					_EtoileEscape_Level4_MAP.GetComponent<Animator> ().enabled=true;
				}
				if (Script_parametreLevel._Afficher_EtoileScore_Level_4 &&
				    _EtoileScore_Level4_MAP.GetComponent<Animator> ().enabled==false)
				{
					_EtoileScore_Level4_MAP.GetComponent<Animator> ().enabled=true;
				}
				if (Script_parametreLevel._Afficher_EtoileTapMouse_Level_4 &&
				    _EtoileTapMouse_Level4_MAP.GetComponent<Animator> ().enabled==false)
				{
					_EtoileTapMouse_Level4_MAP.GetComponent<Animator> ().enabled=true;
				}
			}
			if(Application.loadedLevelName == "scene Stage 5")
			{
				if (Script_parametreLevel._Afficher_EtoileEscape_Level_5 &&
				    _EtoileEscape_Level5_MAP.GetComponent<Animator> ().enabled==false)
				{
					_EtoileEscape_Level5_MAP.GetComponent<Animator> ().enabled=true;
				}
				if (Script_parametreLevel._Afficher_EtoileScore_Level_5 &&
				    _EtoileScore_Level5_MAP.GetComponent<Animator> ().enabled==false)
				{
					_EtoileScore_Level5_MAP.GetComponent<Animator> ().enabled=true;
				}
				if (Script_parametreLevel._Afficher_EtoileTapMouse_Level_5 &&
				    _EtoileTapMouse_Level5_MAP.GetComponent<Animator> ().enabled==false)
				{
					_EtoileTapMouse_Level5_MAP.GetComponent<Animator> ().enabled=true;
				}
			}
			ok=true;
		}
	}
	//
	public void Desactiver_MenuMap()
	{
		Script_Pause.Pause (false);
		_MenuMap.GetComponent<Canvas> ().enabled = false;
	}
	public void StartLevel_MenuMap(string _Level)
	{
		StartCoroutine(Script_TimeLoading.TimeLoadLevel(_Level));
	}
}
