﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Script_Pause : MonoBehaviour 
{
	private static float savedTimeScale=1;
	public static bool isPause;
	
	void Awake()
	{
		Pause (false);
	}
	
	public static void Pause (bool _Pause) 
	{
		if(_Pause == true)
		{
			savedTimeScale = Time.timeScale;
			Time.timeScale = 0;
			AudioListener.pause = true;
		}
		else
		{
			Time.timeScale = savedTimeScale;
			AudioListener.pause = false;
		}
		isPause = _Pause;
	}
}
